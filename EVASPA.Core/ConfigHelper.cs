﻿using System;
using System.Configuration;

namespace EVASPA.Core
{
    public class AppConfig : ISingleton
    {
        public string this[string key]
        {
            get { return ConfigurationManager.AppSettings[key]; }
        }
    }

    public class ConnectionString : ISingleton
    {
        public string this[string key]
        {
            get { return ConfigurationManager.ConnectionStrings[key].ConnectionString; }
        }
    }
}
