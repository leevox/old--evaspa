﻿using Autofac;
using Autofac.Integration.Mvc;
using EVASPA.Core;
using System.Reflection;
using System.Web.Mvc;

namespace EVASPA.Web
{
    public class AutofacRegister
    {
        public void Reg()
        {
            var builder = new ContainerBuilder();

            RegisterAssembly(builder, "EVASPA.Core");
            RegisterAssembly(builder, "EVASPA.Data");
            RegisterAssembly(builder, "EVASPA.Model");
            RegisterAssembly(builder, "EVASPA.Repository");
            RegisterAssembly(builder, "EVASPA.Service");
            RegisterAssembly(builder, "EVASPA.Web");

            builder.RegisterControllers(typeof(MvcApplication).Assembly).PropertiesAutowired();
            builder.RegisterFilterProvider();

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }

        private void RegisterAssembly(ContainerBuilder builder, string assemblyName)
        {
            var assembly = Assembly.Load(assemblyName);

            builder.RegisterAssemblyTypes(assembly)
                .Where(x => typeof(IAutoInstalled).IsAssignableFrom(x))
                .PropertiesAutowired();

            builder.RegisterAssemblyTypes(assembly)
                .Where(x => typeof (IInstancePerHttpRequest).IsAssignableFrom(x))
                .InstancePerRequest();

            builder.RegisterAssemblyTypes(assembly)
                .Where(x => typeof(ISingleton).IsAssignableFrom(x))
                .SingleInstance();
        }
    }
}