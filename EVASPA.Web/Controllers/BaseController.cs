﻿using EVASPA.Core;
using System;
using System.Web.Mvc;

namespace EVASPA.Web.Controllers
{
    [MaintainingActionFilter]
    public abstract class BaseController : Controller
    {
        public ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction("Index", "Home");
        }
    }

    public class MaintainingActionFilter : ActionFilterAttribute
    {
        public AppConfig AppConfig { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (AppConfig["IsMaintaining"].ToInt(0) != 0)
            {
                filterContext.Result = new FilePathResult(@"~\IsMaintaining.html", "text/HTML");
            }
        }
    }
}