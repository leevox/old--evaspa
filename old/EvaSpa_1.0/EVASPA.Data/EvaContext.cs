﻿using EVASPA.Core.UnitOfWork;
using EVASPA.Model.DataModel;
using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;

namespace EVASPA.Data
{
    public class EvaContext : DbContext, IUnitOfWork, IEvaContext
    {
        public EvaContext()
            : base("DefaultConnection")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        #region IEvaContext

        public DbSet<Bill> Bills { get; set; }

        public DbSet<BillItem> BillItems { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Debt> Debts { get; set; }

        public DbSet<DebtHistory> DebtHistories { get; set; }

        public DbSet<Item> Items { get; set; }

        public DbSet<ItemHistory> ItemHistories { get; set; }

        public DbSet<PrepaidCard> PrepaidCards { get; set; }

        public DbSet<Leaving> Leavings { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<PrepaidCardHistory> PrepaidCardHistories { get; set; }

        #endregion

        #region IUnitOfWork

        public TEntity GetEntityInTracker<TEntity>(int id) where TEntity : EntityBase
        {
            var trackerSet = ChangeTracker.Entries<TEntity>();

            TEntity ret = null;
            if (trackerSet.Any(x => x.Entity.Id == id))
                ret= trackerSet.FirstOrDefault(x => x.Entity.Id == id).Entity;

            return ret;
        }

        public DbSet<TEntity> GetEntities<TEntity>() where TEntity : EntityBase
        {
            return Set<TEntity>();
        }

        public void SetState<TEntity>(TEntity entity, EvaEntityState state) where TEntity : EntityBase
        {
            var e = base.Entry(entity);

            switch (state)
            {
                case EvaEntityState.Added:
                    e.State = EntityState.Added;
                    break;
                case EvaEntityState.Deleted:
                    e.State = EntityState.Deleted;
                    break;
                case EvaEntityState.Updated:
                    e.State = EntityState.Modified;
                    break;
                case EvaEntityState.Unchanged:
                    e.State = EntityState.Unchanged;
                    break;
                case EvaEntityState.Detached:
                    e.State = EntityState.Detached;
                    break;
            }
        }

        public new void SaveChanges()
        {
            try
            {
                base.SaveChanges();
            }
            catch (Exception ex)
            {
                
                throw;
            }
        }

        #endregion

        //public DbSet<Product> Products { get; set; }

        //public DbSet<Service> Services { get; set; }

        //public DbSet<CardType> CardTypes { get; set; }
    }
}
