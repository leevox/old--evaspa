﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EVASPA.Model.DataModel
{
    public class BillItemService : BillItem
    {
        public virtual Service Service { get; set; }
        public int ServiceId { get; set; }

        [DisplayName("Sản phẩm dùng kèm")]
        public virtual ICollection<ProductsUsedInService> UsedProducts { get; set; }

        [Required(ErrorMessage = "Chưa nhập người thực hiện.")]
        [DisplayName("Người thực hiện")]
        public virtual ICollection<User> Performers { get; set; }

        public override string ToString()
        {
            return "Dịch vụ - " + Service.Name;
        }
    }
}