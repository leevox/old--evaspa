﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EVASPA.Model.DataModel
{
    public abstract class EntityBase
    {
        [Key]
        [DisplayName("Mã")]
        public int Id { get; set; }

        [ScaffoldColumn(false)]
        public bool IsDeleted { get; set; }
    }
}