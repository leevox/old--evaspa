﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EVASPA.Model.DataModel
{
    public class Customer : EntityBase
    {
        [DisplayName("Tên")]
        [Required(ErrorMessage = "Tên không đúng")]
        public string Name { get; set; }

        [DisplayName("VIP")]
        public bool IsVip { get; set; }

        [DisplayName("Sinh nhật")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Birthday { get; set; }

        [DisplayName("Điện thoại")]
        public string Mobile { get; set; }

        [DisplayName("Email")]
        public string Email { get; set; }

        [DisplayName("Địa chỉ")]
        public string Address { get; set; }

        [DisplayName("Cơ quan")]
        public string Company { get; set; }

        [DisplayName("Ghi chú")]
        [DataType(DataType.MultilineText)]
        public string Note { get; set; }

        public virtual ICollection<Debt> Debts { get; set; }

        public virtual ICollection<DebtHistory> DebtHistories { get; set; }
    }
}