﻿namespace EVASPA.Model.DataModel
{
    public class BillItemCard : BillItem
    {
        public virtual PrepaidCard PrepaidCard { get; set; }
        public int PrepaidCardId { get; set; }

        public override string ToString()
        {
            return (PrepaidCard is PrepaidMoneyCard ? "Phiếu quà tặng - " : "Phiếu dịch vụ - ") +
                   PrepaidCard.SerialNumber;
        }
    }
}