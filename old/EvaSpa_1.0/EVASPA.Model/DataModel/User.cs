﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EVASPA.Model.DataModel
{
    public class User : EntityBase
    {
        [Required(ErrorMessage = "Tài khoản không đúng")]
        [RegularExpression(@"[_\-\w]+", ErrorMessage = "Tài khoản không được có khoảng trắng và ký tự đặc biệt.")]
        [DisplayName("Tên tài khoản")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Tên nhân viên không đúng")]
        [DisplayName("Tên nhân viên")]
        public string DisplayName { get; set; }

        [RegularExpression("\\S*", ErrorMessage = "Mật khẩu không được có khoảng trắng")]
        [DisplayName("Mật khẩu")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DisplayName("Quyền")]
        public UserType UserType { get; set; }

        public virtual ICollection<Leaving> Leaving { get; set; }

        public virtual ICollection<PrepaidServiceCardHistory> ExecutedCards { get; set; }

        public virtual ICollection<BillItemService> ExecutedServices { get; set; }
    }
}