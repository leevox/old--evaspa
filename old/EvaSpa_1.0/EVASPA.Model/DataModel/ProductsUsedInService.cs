﻿
namespace EVASPA.Model.DataModel
{
    public class ProductsUsedInService : EntityBase
    {
        public virtual Product Product { get; set; }
        public int ProductId { get; set; }

        public decimal Quantity { get; set; }
    }
}
