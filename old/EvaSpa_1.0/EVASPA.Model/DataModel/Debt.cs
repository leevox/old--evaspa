﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EVASPA.Model.DataModel
{
    public class Debt : EntityBase
    {
        [DisplayName("Tiền")]
        [DisplayFormat(DataFormatString = "{0:#,#}")]
        [Range(0, int.MaxValue, ErrorMessage = "Tiền không thể nhỏ hơn 0.")]
        public decimal Money { get; set; }

        public virtual Customer Customer { get; set; }
        public int CustomerId { get; set; }
    }
}