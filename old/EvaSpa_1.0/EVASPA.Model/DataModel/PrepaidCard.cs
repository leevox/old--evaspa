﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EVASPA.Model.DataModel
{
    public abstract class PrepaidCard : EntityBase
    {
        [DisplayName("Loại phiếu")]
        public virtual CardType CardType { get; set; }
        public int CardTypeId { get; set; }

        [DisplayName("Khách hàng")]
        public virtual Customer Customer { get; set; }
        public int CustomerId { get; set; }

        [Required]
        [DisplayName("Mã phiếu")]
        public string SerialNumber { get; set; }

        [Required]
        [DisplayName("Ngày bán")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime SoldDate { get; set; }

        [DisplayName("Giá bán")]
        [DisplayFormat(DataFormatString = "{0:#,0}")]
        public decimal SellMoney { get; set; }

        [Required]
        [DisplayName("Hạn mức")]
        [DisplayFormat(DataFormatString = "{0:#,0}")]
        [Range(1, int.MaxValue, ErrorMessage = "Hạn mức phải lớn hơn 0.")]
        public decimal Quota { get; set; }

        [DisplayName("Khóa")]
        public bool IsLocked { get; set; }
    }
}