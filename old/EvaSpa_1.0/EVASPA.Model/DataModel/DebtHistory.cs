﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EVASPA.Model.DataModel
{
    public class DebtHistory : EntityBase
    {
        [DisplayName("Khách hàng")]
        public virtual Customer Customer { get; set; }
        public int CustomerId { get; set; }

        [DisplayName("Ngày")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime Date { get; set; }

        [DisplayName("Tiền")]
        [DisplayFormat(DataFormatString = "{0:#,0}")]
        [Range(1, int.MaxValue, ErrorMessage = "Tiền phải lớn hơn 0.")]
        public decimal Money { get; set; }

        [DisplayName("Ghi chú")]
        [DataType(DataType.MultilineText)]
        public string Note { get; set; }
    }
}