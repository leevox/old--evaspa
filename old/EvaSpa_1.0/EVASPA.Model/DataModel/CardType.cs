﻿using System.ComponentModel.DataAnnotations;

namespace EVASPA.Model.DataModel
{
    public class CardType : Item
    {
        [Range(0, 0, ErrorMessage = "Phiếu không có giá mặc định")]
        [ScaffoldColumn(false)]
        public override decimal DefaultPrice
        {
            get { return 0; }
        }
    }
}