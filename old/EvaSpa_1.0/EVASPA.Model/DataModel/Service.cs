﻿using System.ComponentModel.DataAnnotations;

namespace EVASPA.Model.DataModel
{
    public class Service : Item
    {
        [Range(0, 0, ErrorMessage = "Dịch vụ không có số lượng trong kho")]
        public override decimal QuantityInStock
        {
            get { return 0; }
        }
    }
}