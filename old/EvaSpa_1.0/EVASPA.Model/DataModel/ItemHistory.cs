﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EVASPA.Model.DataModel
{
    public class ItemHistory : EntityBase
    {
        [DisplayName("Sản phẩm")]
        public virtual Item Item { get; set; }
        public int ItemId { get; set; }

        [DisplayName("Ngày nhập")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime Date { get; set; }

        [DisplayName("Số lượng")]
        [DisplayFormat(DataFormatString = "{0:#,0}")]
        [Range(1, int.MaxValue, ErrorMessage = "Số lượng phải lớn hơn 0.")]
        [Required(ErrorMessage = "Số lượng không đúng.")]
        public decimal Quantity { get; set; }

        [DisplayName("Đơn giá")]
        [DisplayFormat(DataFormatString = "{0:#,0}")]
        [Range(0, int.MaxValue, ErrorMessage = "Đơn giá không được là số âm.")]
        [Required(ErrorMessage = "Đơn giá không đúng.")]
        public decimal Price { get; set; }

        [DisplayName("Ghi chú")]
        [DataType(DataType.MultilineText)]
        public string Note { get; set; }
    }
}