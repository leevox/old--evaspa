﻿using System.Collections.Generic;
using System.ComponentModel;

namespace EVASPA.Model.DataModel
{
    public class PrepaidServiceCard : PrepaidCard
    {
        [DisplayName("Dịch vụ")]
        public virtual Service Service { get; set; }
        public int ServiceId { get; set; }

        public virtual ICollection<PrepaidServiceCardHistory> History { get; set; }
    }
}