﻿using System.Collections.Generic;

namespace EVASPA.Model.DataModel
{
    public class PrepaidMoneyCard : PrepaidCard
    {
        public virtual ICollection<PrepaidMoneyCardHistory> History { get; set; }
    }
}