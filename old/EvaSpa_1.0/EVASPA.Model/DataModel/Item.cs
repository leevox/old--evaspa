﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EVASPA.Model.DataModel
{
    public abstract class Item : EntityBase
    {
        [DisplayName("Tên")]
        [Required(ErrorMessage = "Tên không đúng.")]
        public string Name { get; set; }

        [DisplayName("Giá mặc định")]
        [DisplayFormat(DataFormatString = "{0:#,0.#}")]
        [Range(0, int.MaxValue, ErrorMessage = "Giá tiền không được nhỏ hơn 0.")]
        [Required(ErrorMessage = "Giá tiền không đúng.")]
        public virtual decimal DefaultPrice { get; set; }

        [DisplayName("Trong kho")]
        [DisplayFormat(DataFormatString = "{0:#,0.#}")]
        [Range(0, int.MaxValue, ErrorMessage = "Số lượng không được nhỏ hơn 0.")]
        [Required(ErrorMessage = "Số lượng không đúng.")]
        public virtual decimal QuantityInStock { get; set; }

        [DisplayName("Ngừng kinh doanh")]
        [Required(ErrorMessage = "Trạng thái ngừng kinh doanh không đúng.")]
        public bool Discontinued { get; set; }

        [DisplayName("Lịch sử nhập hàng")]
        public virtual ICollection<ItemHistory> ImportHistory { get; set; }
    }
}