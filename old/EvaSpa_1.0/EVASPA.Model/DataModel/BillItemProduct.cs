﻿namespace EVASPA.Model.DataModel
{
    public class BillItemProduct : BillItem
    {
        public virtual Product Product { get; set; }
        public int ProductId { get; set; }

        public override string ToString()
        {
            return "Sản phẩm - " + Product.Name;
        }
    }
}