﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EVASPA.Model.DataModel
{
    public class Leaving : EntityBase
    {
        [DisplayName("Nhân viên")]
        public virtual User User { get; set; }
        public int UserId { get; set; }

        [DisplayName("Ngày nghỉ")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime Date { get; set; }

        [DisplayName("Nghỉ buổi sáng")]
        public bool LeaveOnMorning { get; set; }

        [DisplayName("Nghỉ buổi chiều")]
        public bool LeaveOnAfternoon { get; set; }
    }
}