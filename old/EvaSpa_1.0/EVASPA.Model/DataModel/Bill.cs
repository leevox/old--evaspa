﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EVASPA.Model.DataModel
{
    public class Bill : EntityBase
    {
        [DisplayName("Ngày")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Text)]
        public virtual DateTime Date { get; set; }

        [DisplayName("Khách hàng")]
        public virtual Customer Customer { get; set; }
        public int CustomerId { get; set; }

        [DisplayName("Người bán")]
        public virtual User Seller { get; set; }
        public int SellerId { get; set; }

        [Required(ErrorMessage = "Tiền khách hàng không thể trống")]
        [DisplayName("Tiền khách hàng")]
        [Range(0, int.MaxValue, ErrorMessage = "Tiền khách hàng không được nhỏ hơn 0.")]
        [DisplayFormat(DataFormatString = "{0:#,0}")]
        public decimal CustomerMoney { get; set; }

        [DisplayName("Ghi chú")]
        [DataType(DataType.MultilineText)]
        public string Note { get; set; }

        public virtual ICollection<BillItem> BillItems { get; set; }

        public virtual ICollection<PrepaidMoneyCardHistory> UsedCards { get; set; }

        public virtual Debt Debt { get; set; }
        public int? DebtId { get; set; }
    }
}