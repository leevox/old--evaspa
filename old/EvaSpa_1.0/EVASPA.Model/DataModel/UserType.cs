﻿using System.ComponentModel;

namespace EVASPA.Model.DataModel
{
    public enum UserType
    {
        [Description("Quản trị")]
        Admin = 0,

        [Description("Quản lý")]
        Manager = 1,

        [Description("Nhân viên")]
        Employee = 2
    }
}