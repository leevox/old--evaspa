﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EVASPA.Model.DataModel
{
    public class PrepaidServiceCardHistory : PrepaidCardHistory
    {
        public virtual PrepaidServiceCard PrepaidServiceCard { get; set; }
        public int PrepaidServiceCardId { get; set; }

        [DisplayName("Sản phẩm dùng kèm")]
        public virtual ICollection<ProductsUsedInService> UsedProducts { get; set; }

        [Required(ErrorMessage = "Chưa nhập người thực hiện.")]
        [DisplayName("Người thực hiện")]
        public virtual ICollection<User> Performers { get; set; }

        [Required(ErrorMessage = "Chưa nhập số lượng.")]
        [DisplayName("Số lượng")]
        [DisplayFormat(DataFormatString = "{0:#,0}")]
        [Range(0, int.MaxValue, ErrorMessage = "Số lượng phải lớn hơn 0.")]
        public override decimal UsedAmount { get; set; }
    }
}