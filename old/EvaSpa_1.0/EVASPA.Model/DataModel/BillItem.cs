﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EVASPA.Model.DataModel
{
    public abstract class BillItem : EntityBase
    {
        public virtual Bill Bill { get; set; }
        public int BillId { get; set; }

        [DisplayName("Đơn giá")]
        [DisplayFormat(DataFormatString = "{0:#,0.#}")]
        [Range(0, int.MaxValue, ErrorMessage = "Đơn giá không được nhỏ hơn 0.")]
        public decimal Price { get; set; }

        [DisplayName("Số lượng")]
        [DisplayFormat(DataFormatString = "{0:#,0.#}")]
        [Range(1, int.MaxValue, ErrorMessage = "Số lượng phải lớn hơn 0.")]
        public decimal Quantity { get; set; }

        [NotMapped]
        [DisplayName("Tổng")]
        [DisplayFormat(DataFormatString = "{0:#,0}")]
        public decimal Total
        {
            get { return Price*Quantity; }
        }

        [DisplayName("Giảm giá")]
        [DisplayFormat(DataFormatString = "{0:#,0}")]
        [Range(0, int.MaxValue, ErrorMessage = "Giảm giá không được nhỏ hơn 0.")]
        public decimal? Discount { get; set; }

        [NotMapped]
        [DisplayName("Thành tiền")]
        [DisplayFormat(DataFormatString = "{0:#,0}")]
        public decimal FinalMoney
        {
            get { return Total - (Discount ?? 0); }
        }
    }
}