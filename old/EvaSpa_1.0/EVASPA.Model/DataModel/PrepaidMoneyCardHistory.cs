﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EVASPA.Model.DataModel
{
    public class PrepaidMoneyCardHistory : PrepaidCardHistory
    {
        public virtual PrepaidMoneyCard PrepaidMoneyCard { get; set; }
        public int PrepaidMoneyCardId { get; set; }

        [Required(ErrorMessage = "Chưa nhập tiền.")]
        [DisplayName("Tiền")]
        [DisplayFormat(DataFormatString = "{0:#,0}")]
        [Range(0, int.MaxValue, ErrorMessage = "Tiền phải lớn hơn 0.")]
        public override decimal UsedAmount { get; set; }

        public virtual Bill UsedInBill { get; set; }
        public int UsedInBillId { get; set; }
    }
}