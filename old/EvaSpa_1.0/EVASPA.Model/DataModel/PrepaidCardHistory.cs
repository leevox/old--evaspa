﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EVASPA.Model.DataModel
{
    public abstract class PrepaidCardHistory : EntityBase
    {
        [Required]
        [DisplayName("Đã dùng.")]
        [Range(0, int.MaxValue, ErrorMessage = "Đã dùng không thể nhỏ hơn 0.")]
        public virtual decimal UsedAmount { get; set; }

        [DisplayName("Ngày dùng")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Ngày dùng không đúng")]
        [DataType(DataType.Text)]
        public DateTime Date { get; set; }

        [DisplayName("Ghi chú")]
        [DataType(DataType.MultilineText)]
        public string Note { get; set; }
    }
}