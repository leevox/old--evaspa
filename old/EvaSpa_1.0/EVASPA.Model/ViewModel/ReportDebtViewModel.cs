﻿using EVASPA.Model.DataModel;
using System;
using System.Collections.Generic;

namespace EVASPA.Model.ViewModel
{
    public class ReportDebtViewModel
    {
        public ReportFilterModel FilterModel { get; set; }

        public IEnumerable<ReportLine> Lines { get; set; }

        public class ReportLine
        {
            public bool IsPayDebt { get; set; }

            public DateTime Date { get; set; }
            public int? BillId { get; set; }
            public Customer Customer { get; set; }
            public decimal Money { get; set; }
            public int? DebtHistoryId { get; set; }
            public string Note { get; set; }
        }
    }
}
