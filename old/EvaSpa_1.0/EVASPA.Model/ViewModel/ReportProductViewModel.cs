﻿using EVASPA.Model.DataModel;
using System;
using System.Collections.Generic;

namespace EVASPA.Model.ViewModel
{
    public class ReportProductViewModel
    {
        public ReportFilterModel FilterModel { get; set; }

        public IEnumerable<ReportLine> Lines { get; set; }

        public class ReportLine
        {
            public Type Type { get; set; }

            public DateTime Date { get; set; }
            public Customer Customer { get; set; }
            public int? BillId { get; set; }
            public Item Product { get; set; }
            public string Note { get; set; }

            public decimal Quantity { get; set; }
            public decimal Price { get; set; }
            public decimal Discount { get; set; }

            public decimal Money
            {
                get { return Quantity*Price - Discount; }
            }
        }

        public enum Type
        {
            Import,
            Sell,
            UsedInService,
            UsedInPrepaidServiceCard
        }
    }
}
