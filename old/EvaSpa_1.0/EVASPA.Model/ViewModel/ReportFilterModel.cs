﻿using System;
using System.Collections.Generic;

namespace EVASPA.Model.ViewModel
{
    public enum ReportType
    {
        Employee = 0,
        Product = 1,
        Service = 2,
        PrepaidMoneyCard = 4,
        PrepaidServiceCard = 8,
        Debt = 16
    }

    public class ReportFilterModel
    {
        public bool? ExportToExcel { get; set; }
        public ReportType Type { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public List<int> CustomerIds { get; set; }
        public List<int> ItemIds { get; set; }
        public List<int> EmployeeIds { get; set; } 
    }
}
