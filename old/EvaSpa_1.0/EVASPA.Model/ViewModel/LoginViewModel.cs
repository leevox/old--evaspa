﻿using System.ComponentModel.DataAnnotations;

namespace EVASPA.Model.ViewModel
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Chưa nhập tên tài khoản.")]
        [Display(Name = "Tài khoản")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Chưa nhập mật khẩu.")]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu")]
        public string Password { get; set; }

        [Display(Name = "Tự động đăng nhập")]
        public bool RememberMe { get; set; }
    }
}
