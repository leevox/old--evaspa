﻿using EVASPA.Model.DataModel;
using System;
using System.Collections.Generic;

namespace EVASPA.Model.ViewModel
{
    public class ReportServiceViewModel
    {
        public ReportFilterModel FilterModel { get; set; }

        public IEnumerable<ReportLine> Lines { get; set; }

        public class ReportLine
        {
            public DateTime Date { get; set; }
            public Customer Customer { get; set; }

            public int? BillId { get; set; }
            public PrepaidServiceCard PrepaidServiceCard { get; set; }

            public Service Service { get; set; }
            public string Note { get; set; }

            public ICollection<ProductsUsedInService> UsedProducts { get; set; }
            public ICollection<User> Performers { get; set; }

            public decimal Quantity { get; set; }
            public decimal Price { get; set; }
            public decimal Discount { get; set; }
            public decimal Money { get { return Quantity * Price - Discount; } }
        }
    }
}
