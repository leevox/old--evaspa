﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EVASPA.Model.ViewModel
{
    public class BillViewModel
    {
        [DisplayName("Ngày bán")]
        [Required(ErrorMessage = "Chưa nhập ngày bán")]
        public string Date { get; set; }

        [DisplayName("Người bán")]
        public int SellerId { get; set; }

        [DisplayName("Khách hàng")]
        public int CustomerId { get; set; }

        [DisplayName("Ghi chú")]
        [DataType(DataType.MultilineText)]
        public string Note { get; set; }

        public List<int> UsedMoneyCards { get; set; }

        [DisplayName("Tiền khách hàng")]
        [Range(0,int.MaxValue,ErrorMessage = "Tiền khách hàng không thể âm.")]
        public decimal CustomerMoney { get; set; }

        public List<BillItemProductViewModel> Products { get; set; }
        public List<BillItemServiceViewModel> Services { get; set; }
        public List<BillItemPrepaidMoneyCardViewModel> PrepaidMoneyCards { get; set; }
        public List<BillItemPrepaidServiceCardViewModel> PrepaidServiceCards { get; set; }
    }

    public class BillItemProductViewModel
    {
        [DisplayName("Sản phẩm")]
        public int ProductId { get; set; }

        [DisplayName("Số lượng")]
        public decimal Quantity { get; set; }

        [DisplayName("Đơn giá")]
        public decimal Price { get; set; }

        [DisplayName("Giảm giá")]
        public decimal? Discount { get; set; }
    }

    public class BillItemServiceViewModel
    {
        [DisplayName("Dịch vụ")]
        public int ServiceId { get; set; }

        [DisplayName("Nhân viên thực hiện")]
        public List<int> PerformerIds { get; set; }

        [DisplayName("Sản phẩm dùng kèm")]
        public List<int> UsedProductIds { get; set; }

        public List<int> UsedProductCounts { get; set; }

        [DisplayName("Số lượng")]
        public decimal Quantity { get; set; }

        [DisplayName("Đơn giá")]
        public decimal Price { get; set; }

        [DisplayName("Giảm giá")]
        public decimal? Discount { get; set; }
    }

    public class BillItemPrepaidMoneyCardViewModel
    {
        [DisplayName("Loại phiếu")]
        public int CardTypeId { get; set; }

        [DisplayName("Hạn mức")]
        public decimal Quota { get; set; }

        [DisplayName("Giảm giá")]
        public decimal? Discount { get; set; }
    }

    public class BillItemPrepaidServiceCardViewModel
    {
        [DisplayName("Loại phiếu")]
        public int CardTypeId { get; set; }

        [DisplayName("Dịch vụ")]
        public int ServiceId { get; set; }

        [DisplayName("Hạn mức")]
        public decimal Quota { get; set; }

        [DisplayName("Đơn giá")]
        public decimal Price { get; set; }

        [DisplayName("Giảm giá")]
        public decimal? Discount { get; set; }
    }
}