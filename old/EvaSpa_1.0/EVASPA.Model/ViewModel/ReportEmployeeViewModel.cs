﻿using EVASPA.Model.DataModel;
using System;
using System.Collections.Generic;

namespace EVASPA.Model.ViewModel
{
    public class ReportEmployeeViewModel
    {
        public ReportFilterModel FilterModel { get; set; }

        public Dictionary<User, List<Leaving>> UserLeaving { get; set; }
        public Dictionary<DateTime, Dictionary<User, Leaving>> LeavingByDate { get; set; }

        public List<Service> Services { get; set; }
        public IEnumerable<User> Users { get; set; }

        // <ServiceId, UserId>, Count
        public Dictionary<Tuple<int, int>, float> Status { get; set; }

        public void AddStatus(Service svc, User usr, float count)
        {
            var key = Tuple.Create(svc.Id, usr.Id);
            if (Status.ContainsKey(key))
            {
                Status[key] += count;
            }
            else
            {
                Status.Add(key, count);
            }
        }
    }
}
