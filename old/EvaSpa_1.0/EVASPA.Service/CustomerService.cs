﻿using EVASPA.Core.Repository;
using EVASPA.Core.Service;
using EVASPA.Core.UnitOfWork;
using EVASPA.Model.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EVASPA.Service
{
    public class CustomerService : ICustomerService
    {
        private ICustomerRepository CustomerRepository { get; set; }
        private IUnitOfWork UnitOfWork { get; set; }
        private CommonService<Customer> CommonService { get; set; }

        public CustomerService(IUnitOfWork unitOfWork, ICustomerRepository customerRepository)
        {
            UnitOfWork = unitOfWork;
            CustomerRepository = customerRepository;
            CommonService = new CommonService<Customer>(UnitOfWork, CustomerRepository);
        }

        public IQueryable<Customer> AllEntities
        {
            get { return CommonService.AllEntities; }
        }

        public IQueryable<Customer> AllEntitiesIncludedDeleted
        {
            get { return CommonService.AllEntitiesIncludedDeleted; }
        }

        public Customer GetById(int? entityId, bool includeDeleted = false)
        {
            return CommonService.GetById(entityId, includeDeleted);
        }

        public Customer Add(Customer entity)
        {
            return CommonService.Add(entity);
        }

        public Customer Update(Customer entity)
        {
            return CommonService.Update(entity);
        }

        public void Delete(Customer entity)
        {
            CommonService.Delete(entity);
        }

        public void Delete(int entityId)
        {
            CommonService.Delete(entityId);
        }

        public IQueryable<Customer> GetAllCustomerWhoHasBirthdayIsInDay(DateTime day)
        {
            return CustomerRepository.Where(x => x.Birthday.HasValue && x.Birthday.Value.Month == day.Month && x.Birthday.Value.Day == day.Day);
        }

        public IQueryable<Customer> GetAllCustomerWhoHasBirthdayIsInRange(DateTime from, DateTime to)
        {
            return CustomerRepository.Where(x =>
                x.Birthday.HasValue &&
                !(
                    // before from
                    (x.Birthday.Value.Month < from.Month || (x.Birthday.Value.Month == from.Month && x.Birthday.Value.Day <= from.Day))
                    ||
                    // after to
                    (x.Birthday.Value.Month > to.Month || (x.Birthday.Value.Month == to.Month && x.Birthday.Value.Day > to.Day))
                )).OrderBy(x=>x.Birthday);
        }

        public IQueryable<Customer> AllCustomersWithDebts()
        {
            return CustomerRepository.AllCustomersWithDebt();
        }

        public decimal GetCurrentDebt(Customer customer)
        {
            var totalDebt = customer.Debts.Where(x => !x.IsDeleted).Sum(x => x.Money);
            var totalPayDebt = customer.DebtHistories.Where(x => !x.IsDeleted).Sum(x => x.Money);
            return totalPayDebt - totalDebt;
        }
    }
}
