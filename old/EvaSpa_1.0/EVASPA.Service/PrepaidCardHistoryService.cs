using EVASPA.Core.Repository;
using EVASPA.Core.Service;
using EVASPA.Core.UnitOfWork;
using System.Linq;
using EVASPA.Model.DataModel;

namespace EVASPA.Service
{
    public class PrepaidCardHistoryService : IPrepaidCardHistoryService
    {
        private IPrepaidCardHistoryRepository PrepaidCardHistoryRepository { get; set; }
        private IUnitOfWork UnitOfWork { get; set; }
        private CommonService<PrepaidCardHistory> CommonService { get; set; }

        public PrepaidCardHistoryService(IUnitOfWork unitOfWork, IPrepaidCardHistoryRepository prepaidCardHistoryRepository)
        {
            UnitOfWork = unitOfWork;
            PrepaidCardHistoryRepository = prepaidCardHistoryRepository;
            CommonService = new CommonService<PrepaidCardHistory>(UnitOfWork, PrepaidCardHistoryRepository);
        }

        public IQueryable<PrepaidCardHistory> AllEntities
        {
            get { return CommonService.AllEntities; }
        }

        public IQueryable<PrepaidCardHistory> AllEntitiesIncludedDeleted
        {
            get { return CommonService.AllEntitiesIncludedDeleted; }
        }

        public PrepaidCardHistory GetById(int? entityId, bool includeDeleted = false)
        {
            return CommonService.GetById(entityId, includeDeleted);
        }

        public PrepaidCardHistory Add(PrepaidCardHistory entity)
        {
            return CommonService.Add(entity);
        }

        public PrepaidCardHistory Update(PrepaidCardHistory entity)
        {
            return CommonService.Update(entity);
        }

        public void Delete(PrepaidCardHistory entity)
        {
            CommonService.Delete(entity);
        }

        public void Delete(int entityId)
        {
            CommonService.Delete(entityId);
        }
    }
}