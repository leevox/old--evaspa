using EVASPA.Core.Repository;
using EVASPA.Core.Service;
using EVASPA.Core.UnitOfWork;
using System.Linq;
using EVASPA.Model.DataModel;

namespace EVASPA.Service
{
    public class DebtService : IDebtService
    {
        private IDebtRepository DebtRepository { get; set; }
        private IDebtHistoryRepository DebtHistoryRepository { get; set; }
        private IUnitOfWork UnitOfWork { get; set; }
        private CommonService<Debt> CommonService { get; set; }

        public DebtService(IUnitOfWork unitOfWork, IDebtRepository debtRepository, IDebtHistoryRepository debtHistoryRepository)
        {
            UnitOfWork = unitOfWork;
            DebtRepository = debtRepository;
            DebtHistoryRepository = debtHistoryRepository;
            CommonService = new CommonService<Debt>(UnitOfWork, DebtRepository);
        }

        public IQueryable<Debt> AllEntities
        {
            get { return CommonService.AllEntities; }
        }

        public IQueryable<Debt> AllEntitiesIncludedDeleted
        {
            get { return CommonService.AllEntitiesIncludedDeleted; }
        }

        public Debt GetById(int? entityId, bool includeDeleted = false)
        {
            return CommonService.GetById(entityId, includeDeleted);
        }

        public Debt Add(Debt entity)
        {
            return CommonService.Add(entity);
        }

        public Debt Update(Debt entity)
        {
            return CommonService.Update(entity);
        }

        public void Delete(Debt entity)
        {
            CommonService.Delete(entity);
        }

        public void Delete(int entityId)
        {
            CommonService.Delete(entityId);
        }

        public void PayDebt(DebtHistory model)
        {
            DebtHistoryRepository.Add(model);
            UnitOfWork.SaveChanges();
        }

        public DebtHistory GetDebtHistoryById(int? id)
        {
            return DebtHistoryRepository.FirstOrDefault(x => x.Id == id);
        }

        public void DeleteDebtHistory(int id)
        {
            DebtHistoryRepository.Delete(id);
            UnitOfWork.SaveChanges();
        }
    }
}