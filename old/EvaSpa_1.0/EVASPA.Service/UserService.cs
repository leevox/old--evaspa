﻿using System.Collections.Generic;
using EVASPA.Core.Repository;
using EVASPA.Core.Service;
using EVASPA.Core.UnitOfWork;
using System;
using System.Linq;
using EVASPA.Model.DataModel;

namespace EVASPA.Service
{
    public class UserService : IUserService
    {
        private IUnitOfWork UnitOfWork { get; set; }
        private CommonService<User> CommonService { get; set; }
        private IUserRepository UserRepository { get; set; }
        private ILeavingRepository LeavingRepository { get; set; }

        public UserService(IUnitOfWork unitOfWork, IUserRepository userRepository, ILeavingRepository leavingRepository)
        {
            UnitOfWork = unitOfWork;
            UserRepository = userRepository;
            LeavingRepository = leavingRepository;
            CommonService = new CommonService<User>(UnitOfWork, UserRepository);
        }

        public IQueryable<User> AllEntities
        {
            get { return CommonService.AllEntities; }
        }

        public IQueryable<User> AllEntitiesIncludedDeleted
        {
            get { return CommonService.AllEntitiesIncludedDeleted; }
        }

        public User GetById(int? entityId, bool includeDeleted = false)
        {
            return CommonService.GetById(entityId, includeDeleted);
        }

        public User Add(User entity)
        {
            var isExisted =
                   UserRepository.Any(
                       x => String.Compare(x.UserName, entity.UserName, StringComparison.OrdinalIgnoreCase) == 0);

            if (isExisted)
            {
                throw new Exception("Tên tài khoản đã tồn tại.");
            }
            return CommonService.Add(entity);
        }

        public User Update(User entity)
        {
            var model = GetById(entity.Id);
            if (UserRepository.Any(x => x.Id != model.Id && string.Compare(x.UserName, entity.UserName, StringComparison.OrdinalIgnoreCase) == 0))
            {
                throw new Exception("Tên tài khoản đã tồn tại.");
            }
            else
            {
                model.UserName = entity.UserName;
                model.DisplayName = entity.DisplayName;
                if (!string.IsNullOrWhiteSpace(entity.Password))
                    model.Password = entity.Password;
                model.UserType = entity.UserType;
            }
            UnitOfWork.SaveChanges();
            return model;
            //return CommonService.Update(entity);
        }

        public void Delete(User entity)
        {
            CommonService.Delete(entity);
        }

        public void Delete(int entityId)
        {
            CommonService.Delete(entityId);
        }

        public User ValidateUser(string userName, string password)
        {
            userName = userName.Trim();
            password = password.Trim();
            return
                UserRepository.FirstOrDefault(
                    x =>
                        string.Compare(x.UserName, userName, StringComparison.OrdinalIgnoreCase) == 0 &&
                        string.Compare(x.Password, password, StringComparison.Ordinal) == 0);
        }

        public IEnumerable<User> AllEmployees
        {
            get { return UserRepository.Where(x => x.UserType == UserType.Employee); }
        }

        public void TakeLeave(Leaving model)
        {
            LeavingRepository.Add(model);
            UnitOfWork.SaveChanges();
        }
    }
}
