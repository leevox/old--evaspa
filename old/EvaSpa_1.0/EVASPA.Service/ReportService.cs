﻿using EVASPA.Core.Repository;
using EVASPA.Core.Service;
using EVASPA.Core.Utils;
using EVASPA.Model.DataModel;
using EVASPA.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace EVASPA.Service
{
    public class ReportService : IReportService
    {
        private IItemHistoryRepository ItemHistoryRepository { get; set; }
        private IBillRepository BillRepository { get; set; }
        private IPrepaidCardHistoryRepository PrepaidCardHistoryRepository { get; set; }
        private IUserRepository UserRepository { get; set; }
        private ILeavingRepository LeavingRepository { get; set; }
        private IDebtHistoryRepository DebtHistoryRepository { get; set; }

        public ReportService(IItemHistoryRepository itemHistoryRepository, IBillRepository billRepository,
            IPrepaidCardHistoryRepository prepaidCardHistoryRepository, IUserRepository userRepository,
            ILeavingRepository leavingRepository, IDebtHistoryRepository debtHistoryRepository)
        {
            ItemHistoryRepository = itemHistoryRepository;
            BillRepository = billRepository;
            PrepaidCardHistoryRepository = prepaidCardHistoryRepository;
            UserRepository = userRepository;
            LeavingRepository = leavingRepository;
            DebtHistoryRepository = debtHistoryRepository;
        }

        public ReportEmployeeViewModel GetReportEmployee(ReportFilterModel filter)
        {
            var ret = new ReportEmployeeViewModel { FilterModel = filter };

            ret.Users = UserRepository.AllEntities.Where(x => x.UserType == UserType.Employee).ToList();
            ret.Services = new List<Model.DataModel.Service>();
            ret.Status = new Dictionary<Tuple<int, int>, float>();

            #region leaving

            ret.UserLeaving = new Dictionary<User, List<Leaving>>();
            ret.LeavingByDate = new Dictionary<DateTime, Dictionary<User, Leaving>>();
            var allLeavings = LeavingRepository.Where(x => x.Date >= filter.StartDate && x.Date <= filter.EndDate && x.User.UserType == UserType.Employee).ToList();

            foreach (var user in ret.Users)
            {
                var tmp = allLeavings.Where(x => x.UserId == user.Id);
                ret.UserLeaving.Add(user, tmp.ToList());
                foreach(var leaving in tmp)
                {
                    if (!ret.LeavingByDate.ContainsKey(leaving.Date))
                    {
                        ret.LeavingByDate.Add(leaving.Date, new Dictionary<User, Leaving>());
                    }
                    if (!ret.LeavingByDate[leaving.Date].ContainsKey(leaving.User))
                    {
                        ret.LeavingByDate[leaving.Date].Add(leaving.User, leaving);
                    }
                }
            }

            #endregion

            #region working

            foreach (var user in ret.Users)
            {
                var services = user.ExecutedServices.Where(x=>!x.IsDeleted && x.Bill.Date >= filter.StartDate && x.Bill.Date <= filter.EndDate);
                foreach (var svc in services)
                {
                    if (!ret.Services.Contains(svc.Service))
                    {
                        ret.Services.Add(svc.Service);
                    }
                    ret.AddStatus(svc.Service, user, (float) svc.Quantity/svc.Performers.Count);
                }

                var cards = user.ExecutedCards.Where(x => !x.IsDeleted && x.Date >= filter.StartDate && x.Date <= filter.EndDate);
                foreach (var card in cards)
                {
                    if (!ret.Services.Contains(card.PrepaidServiceCard.Service))
                    {
                        ret.Services.Add(card.PrepaidServiceCard.Service);
                    }
                    ret.AddStatus(card.PrepaidServiceCard.Service, user, (float) card.UsedAmount/card.Performers.Count);
                }
            }

            #endregion

            return ret;
        }
        public byte[] GetExcelEmployee(ReportEmployeeViewModel model)
        {
            var excel = new ExcelHelper("Nghỉ phép");
            
            var users = model.Users.Select(x => x.DisplayName).ToArray();

            #region leaving

            excel.SetValue("B2", "Nghỉ phép");
            excel.SetDisplayStyle("B2", new ExcelHelper.CellStyle { FontSize = 16, FontStyle = ExcelHelper.FontStyle.Bold, FontColor = Color.Red });

            excel.SetValue("C3", model.FilterModel.StartDate);
            excel.SetValue("B3", "Từ ngày");
            excel.SetDisplayStyle("B3", new ExcelHelper.CellStyle { FontStyle = ExcelHelper.FontStyle.Bold });

            excel.SetValue("C4", model.FilterModel.EndDate);
            excel.SetValue("B4", "Đến ngày");
            excel.SetDisplayStyle("B4", new ExcelHelper.CellStyle { FontStyle = ExcelHelper.FontStyle.Bold });

            excel.SetValuesInRow("C6", users);

            int rowIndex = 7;
            foreach(var key in model.LeavingByDate.Keys.OrderBy(x=>x))
            {
                excel.SetValue("B" + rowIndex, key);
                List<string> tmp1 = new List<string>();
                foreach(var user in model.Users)
                {
                    if (model.LeavingByDate[key].ContainsKey(user))
                    {
                        if (model.LeavingByDate[key][user].LeaveOnMorning && model.LeavingByDate[key][user].LeaveOnAfternoon)
                        {
                            tmp1.Add("Sáng, Chiều");
                        }
                        else if (model.LeavingByDate[key][user].LeaveOnMorning)
                        {
                            tmp1.Add("Sáng");
                        }
                        else if (model.LeavingByDate[key][user].LeaveOnAfternoon)
                        {
                            tmp1.Add("Chiều");
                        }
                        else
                        {
                            tmp1.Add("");
                        }
                    }
                    else
                    {
                        tmp1.Add("");
                    }
                }
                excel.SetValuesInRow("C" + rowIndex, tmp1.ToArray());
                rowIndex++;
            }

            List<object> morning = new List<object>();
            List<object> afternoon = new List<object>();
            foreach (var user in model.Users)
            {
                morning.Add(model.UserLeaving[user].Count(x => x.LeaveOnMorning));
                afternoon.Add(model.UserLeaving[user].Count(x => x.LeaveOnAfternoon));
            }

            excel.SetValue("B" + rowIndex, "Nghỉ buổi sáng");
            excel.SetValuesInRow("C" + rowIndex, morning.ToArray());
            rowIndex++;
            excel.SetValue("B" + rowIndex, "Nghỉ buổi chiều");
            excel.SetValuesInRow("C" + rowIndex, afternoon.ToArray());

            #endregion

            excel.AddSheet("Làm việc");
            excel.SetActiveSheet("Làm việc");

            #region working

            excel.SetValue("B2", "Làm việc");
            excel.SetDisplayStyle("B2", new ExcelHelper.CellStyle { FontSize = 16, FontStyle = ExcelHelper.FontStyle.Bold, FontColor = Color.Red });

            excel.SetValue("C3", model.FilterModel.StartDate);
            excel.SetValue("B3", "Từ ngày");
            excel.SetDisplayStyle("B3", new ExcelHelper.CellStyle { FontStyle = ExcelHelper.FontStyle.Bold });

            excel.SetValue("C4", model.FilterModel.EndDate);
            excel.SetValue("B4", "Đến ngày");
            excel.SetDisplayStyle("B4", new ExcelHelper.CellStyle { FontStyle = ExcelHelper.FontStyle.Bold });

            excel.SetValuesInRow("C6", users);

            List<object> tmp;
            const int startRow = 7;
            var row = startRow;
            foreach(var svc in model.Services.OrderBy(x=>x.Name))
            {
                tmp = new List<object>();
                foreach(var user in model.Users)
                {
                    var x = Tuple.Create(svc.Id, user.Id);
                    tmp.Add(model.Status.ContainsKey(x) ? model.Status[x] : 0);
                }
                excel.SetValue("B" + row, svc.Name);
                excel.SetValuesInRow("C" + row, tmp.ToArray());
                row++;
            }

            #endregion

            return excel.GetContent();
        }

        public ReportProductViewModel GetReportProduct(ReportFilterModel filter)
        {
            var ret = new ReportProductViewModel { FilterModel = filter };

            var lines = new List<ReportProductViewModel.ReportLine>();

            #region Import Product

            var histories = ItemHistoryRepository.AllEntities.Where(x => x.Date >= filter.StartDate && x.Date <= filter.EndDate);
            if (filter.ItemIds != null && filter.ItemIds.Any())
            {
                histories = histories.Where(x => filter.ItemIds.Contains(x.ItemId));
            }

            lines.AddRange(histories.ToList().Select(history => new ReportProductViewModel.ReportLine
            {
                Type = ReportProductViewModel.Type.Import,
                Date = history.Date,
                Customer = null,
                BillId = null,
                Product = history.Item,
                Note = history.Note,
                Quantity = history.Quantity,
                Price = -history.Price
            }));

            #endregion

            #region sell product

            var bills = BillRepository.AllEntities.Where(x => x.Date >= filter.StartDate && x.Date <= filter.EndDate);
            foreach (var bill in bills.ToList())
            {
                var billItems = bill.BillItems.OfType<BillItemProduct>().Where(x => !x.IsDeleted);

                lines.AddRange(billItems.Select(billItem => new ReportProductViewModel.ReportLine
                {
                    Type = ReportProductViewModel.Type.Sell,
                    Date = bill.Date,
                    Customer = bill.Customer,
                    BillId = bill.Id,
                    Product = billItem.Product,
                    Note = bill.Note,
                    Quantity = billItem.Quantity,
                    Price = billItem.Price,
                    Discount = billItem.Discount ?? 0
                }));
            }

            #endregion

            #region product used in bill item service

            foreach (var bill in bills.ToList())
            {
                var billItems = bill.BillItems.OfType<Model.DataModel.BillItemService>().Where(x => !x.IsDeleted && x.UsedProducts != null && x.UsedProducts.Any());
                foreach (var billItem in billItems)
                {
                    lines.AddRange(billItem.UsedProducts.Select(product => new ReportProductViewModel.ReportLine
                    {
                        Type = ReportProductViewModel.Type.UsedInService,
                        Date = bill.Date,
                        Customer = bill.Customer,
                        BillId = bill.Id,
                        Product = product.Product,
                        Note = string.Format("Dùng kèm dịch vụ '{0}'", billItem.Service.Name),
                        Quantity = product.Quantity,
                        Price = 0,
                        Discount = 0
                    }));
                }
            }

            #endregion

            #region product used in prepaid service card

            var prepaidServiceCardHistories = PrepaidCardHistoryRepository.AllEntities.Where(x => x.Date >= filter.StartDate && x.Date <= filter.EndDate).ToList().OfType<PrepaidServiceCardHistory>().Where(x => x.UsedProducts != null && x.UsedProducts.Any());
            foreach (var history in prepaidServiceCardHistories)
            {
                lines.AddRange(history.UsedProducts.Select(product => new ReportProductViewModel.ReportLine
                {
                    Type = ReportProductViewModel.Type.UsedInPrepaidServiceCard,
                    Date = history.Date,
                    Customer = history.PrepaidServiceCard.Customer,
                    BillId = null,
                    Product = product.Product,
                    Note = string.Format("Dùng kèm phiếu '{0}'", history.PrepaidServiceCard.SerialNumber),
                    Quantity = product.Quantity,
                    Price = 0,
                    Discount = 0
                }));
            }

            #endregion

            ret.Lines = lines.OrderByDescending(x => x.Date);

            return ret;
        }
        public byte[] GetExcelProduct(ReportProductViewModel model)
        {
            var excel = new ExcelHelper("Sản phẩm");

            excel.SetValue("B2", "Báo cáo sản phẩm");
            excel.SetDisplayStyle("B2", new ExcelHelper.CellStyle { FontSize = 16, FontStyle = ExcelHelper.FontStyle.Bold, FontColor = Color.Red });

            excel.SetValue("C3", model.FilterModel.StartDate);
            excel.SetValue("B3", "Từ ngày");
            excel.SetDisplayStyle("B3", new ExcelHelper.CellStyle { FontStyle = ExcelHelper.FontStyle.Bold });

            excel.SetValue("C4", model.FilterModel.EndDate);
            excel.SetValue("B4", "Đến ngày");
            excel.SetDisplayStyle("B4", new ExcelHelper.CellStyle { FontStyle = ExcelHelper.FontStyle.Bold });

            var headers = new string[] { "Ngày", "Hóa đơn", "Hình thức", "Khách hàng", "Mã khách hàng", "Sản phẩm", "Mã sản phẩm", "Ghi chú", "Số lượng", "Đơn giá", "Giảm giá", "Thành tiền" };
            excel.SetValuesInRow("B6", headers);

            const int startRow = 7;
            //const int startCol = 2;

            var row = startRow;
            foreach(var line in model.Lines)
            {
                excel.SetValuesInRow("B" + row,
                    line.Date,
                    string.Format("{0:000000}", line.BillId),
                    line.Type == ReportProductViewModel.Type.Import ? "Nhập hàng" : line.Type == ReportProductViewModel.Type.Sell ? "Bán hàng" : "Dùng kèm",
                    GetCustomerName(line.Customer),
                    (line.Customer != null ? line.Customer.Id : 0),
                    line.Product.Name,
                    line.Product.Id,
                    line.Note,
                    line.Quantity,
                    line.Price,
                    line.Discount,
                    line.Money
                );
                row++;
            }

            return excel.GetContent();
        }

        public ReportServiceViewModel GetReportService(ReportFilterModel filter)
        {
            var ret = new ReportServiceViewModel { FilterModel = filter };

            var lines = new List<ReportServiceViewModel.ReportLine>();

            #region sell service

            var bills = BillRepository.AllEntities.Where(x => x.Date >= filter.StartDate && x.Date <= filter.EndDate);
            foreach (var bill in bills.ToList())
            {
                var billItems = bill.BillItems.OfType<Model.DataModel.BillItemService>().Where(x => !x.IsDeleted);

                lines.AddRange(billItems.Select(billItem => new ReportServiceViewModel.ReportLine
                {
                    Date = bill.Date,
                    Customer = bill.Customer,
                    BillId = bill.Id,
                    Service = billItem.Service,
                    Performers = billItem.Performers,
                    UsedProducts = billItem.UsedProducts,
                    Note = bill.Note,
                    Quantity = billItem.Quantity,
                    Price = billItem.Price,
                    Discount = billItem.Discount ?? 0
                }));
            }

            #endregion

            #region use service by prepaid service card

            var histories = PrepaidCardHistoryRepository.AllEntities.Where(x => x.Date >= filter.StartDate && x.Date <= filter.EndDate).OfType<PrepaidServiceCardHistory>().Where(x => !x.PrepaidServiceCard.IsDeleted);
            lines.AddRange(histories.ToList().Select(history => new ReportServiceViewModel.ReportLine
            {
                Date = history.Date,
                Customer = history.PrepaidServiceCard.Customer,
                PrepaidServiceCard = history.PrepaidServiceCard,
                Service = history.PrepaidServiceCard.Service,
                Performers = history.Performers,
                UsedProducts = history.UsedProducts,
                Note = history.Note,
                Quantity = history.UsedAmount,
                Price = history.PrepaidServiceCard.SellMoney / history.PrepaidServiceCard.Quota,
            }));

            #endregion

            ret.Lines = lines.OrderByDescending(x => x.Date);

            return ret;
        }
        public byte[] GetExcelService(ReportServiceViewModel model)
        {
            var excel = new ExcelHelper("Dịch vụ");

            excel.SetValue("B2", "Báo cáo dịch vụ");
            excel.SetDisplayStyle("B2", new ExcelHelper.CellStyle { FontSize = 16, FontStyle = ExcelHelper.FontStyle.Bold, FontColor = Color.Red });

            excel.SetValue("C3", model.FilterModel.StartDate);
            excel.SetValue("B3", "Từ ngày");
            excel.SetDisplayStyle("B3", new ExcelHelper.CellStyle { FontStyle = ExcelHelper.FontStyle.Bold });

            excel.SetValue("C4", model.FilterModel.EndDate);
            excel.SetValue("B4", "Đến ngày");
            excel.SetDisplayStyle("B4", new ExcelHelper.CellStyle { FontStyle = ExcelHelper.FontStyle.Bold });

            var headers = new string[] { "Ngày", "Hóa đơn", "Phiếu", "Khách hàng", "Mã khách hàng", "Dịch vụ", "Mã dịch vụ", "Ghi chú", "Nhân viên thực hiện", "Sản phẩm dùng kèm", "Số suất", "Đơn giá", "Giảm giá", "Thành tiền" };
            excel.SetValuesInRow("B6", headers);

            const int startRow = 7;
            //const int startCol = 2;

            var row = startRow;
            foreach (var line in model.Lines)
            {
                excel.SetValuesInRow("B" + row,
                    line.Date,
                    line.BillId,
                    line.PrepaidServiceCard != null ? line.PrepaidServiceCard.SerialNumber : string.Empty,
                    GetCustomerName(line.Customer),
                    (line.Customer != null ? line.Customer.Id : 0),
                    line.Service.Name,
                    line.Service.Id,
                    line.Note,
                    string.Join(", ", (line.Performers ?? Enumerable.Empty<User>()).Select(x => x.DisplayName)),
                    string.Join(", ", (line.UsedProducts ?? Enumerable.Empty<ProductsUsedInService>()).Select(x => x.Product.Name + " (" + x.Quantity + ")")),
                    line.Quantity,
                    line.Price,
                    line.Discount,
                    line.Money
                );
                row++;
            }

            return excel.GetContent();
        }

        public ReportPrepaidMoneyCardViewModel GetReportPrepaidMoneyCard(ReportFilterModel filter)
        {
            var ret = new ReportPrepaidMoneyCardViewModel { FilterModel = filter };

            var lines = new List<ReportPrepaidMoneyCardViewModel.ReportLine>();

            var bills = BillRepository.AllEntities.Where(x => x.Date >= filter.StartDate && x.Date <= filter.EndDate);
            foreach (var bill in bills.ToList())
            {
                #region use card

                if (bill.UsedCards != null && bill.UsedCards.Any())
                {
                    lines.AddRange(bill.UsedCards.Select(cardHistory => new ReportPrepaidMoneyCardViewModel.ReportLine
                    {
                        UseType = ReportPrepaidMoneyCardViewModel.UseType.Use,
                        Date = bill.Date,
                        Customer = bill.Customer,
                        BillId = bill.Id,
                        PrepaidMoneyCard = cardHistory.PrepaidMoneyCard,
                        Note = bill.Note,
                        Quantity = 1,
                        Price = cardHistory.UsedAmount,
                        Discount = 0
                    }));
                }

                #endregion

                #region sell card

                var billItems = bill.BillItems.OfType<BillItemCard>().Where(x => !x.IsDeleted && x.PrepaidCard is PrepaidMoneyCard);

                lines.AddRange(billItems.Select(billItem => new ReportPrepaidMoneyCardViewModel.ReportLine
                {
                    UseType = ReportPrepaidMoneyCardViewModel.UseType.Sell,
                    Date = bill.Date,
                    Customer = bill.Customer,
                    BillId = bill.Id,
                    PrepaidMoneyCard = billItem.PrepaidCard as PrepaidMoneyCard,
                    Note = bill.Note,
                    Quantity = billItem.Quantity,
                    Price = billItem.Price,
                    Discount = billItem.Discount ?? 0
                }));

                #endregion
            }

            ret.Lines = lines.OrderByDescending(x => x.Date);
            return ret;
        }
        public byte[] GetExcelPrepaidMoneyCard(ReportPrepaidMoneyCardViewModel model)
        {
            var excel = new ExcelHelper("Phiếu quà tặng");

            excel.SetValue("B2", "Báo cáo phiếu quà tặng");
            excel.SetDisplayStyle("B2", new ExcelHelper.CellStyle { FontSize = 16, FontStyle = ExcelHelper.FontStyle.Bold, FontColor = Color.Red });

            excel.SetValue("C3", model.FilterModel.StartDate);
            excel.SetValue("B3", "Từ ngày");
            excel.SetDisplayStyle("B3", new ExcelHelper.CellStyle { FontStyle = ExcelHelper.FontStyle.Bold });

            excel.SetValue("C4", model.FilterModel.EndDate);
            excel.SetValue("B4", "Đến ngày");
            excel.SetDisplayStyle("B4", new ExcelHelper.CellStyle { FontStyle = ExcelHelper.FontStyle.Bold });

            var headers = new string[] { "Ngày", "Hóa đơn", "Loại phiếu", "Phiếu", "Khóa", "Hình thức", "Khách hàng", "Mã khách hàng", "Ghi chú", "Tiền", "Giảm giá", "Giá bán" };
            excel.SetValuesInRow("B6", headers);

            const int startRow = 7;
            //const int startCol = 2;

            var row = startRow;
            foreach (var line in model.Lines)
            {
                excel.SetValuesInRow("B" + row,
                    line.Date,
                    line.BillId,
                    line.PrepaidMoneyCard != null ? line.PrepaidMoneyCard.CardType.Name : string.Empty,
                    line.PrepaidMoneyCard != null ? line.PrepaidMoneyCard.SerialNumber : string.Empty,
                    line.PrepaidMoneyCard != null && line.PrepaidMoneyCard.IsLocked ? "Đã khóa" : string.Empty,
                    line.UseType == ReportPrepaidMoneyCardViewModel.UseType.Import ? "Nhập" : line.UseType == ReportPrepaidMoneyCardViewModel.UseType.Sell ? "Bán" : "Sử dụng",
                    GetCustomerName(line.Customer),
                    (line.Customer != null ? line.Customer.Id : 0),
                    line.Note,
                    line.Price,
                    line.Discount,
                    line.Money
                );
                row++;
            }

            return excel.GetContent();
        }

        public ReportPrepaidServiceCardViewModel GetReportPrepaidServiceCard(ReportFilterModel filter)
        {
            var ret = new ReportPrepaidServiceCardViewModel { FilterModel = filter };

            var lines = new List<ReportPrepaidServiceCardViewModel.ReportLine>();

            #region sell card

            var bills = BillRepository.AllEntities.Where(x => x.Date >= filter.StartDate && x.Date <= filter.EndDate);
            foreach (var bill in bills.ToList())
            {
                var billItems = bill.BillItems.OfType<BillItemCard>().Where(x => !x.IsDeleted && x.PrepaidCard is PrepaidServiceCard);

                lines.AddRange(from billItem in billItems
                               let card = billItem.PrepaidCard as PrepaidServiceCard
                               select new ReportPrepaidServiceCardViewModel.ReportLine
                               {
                                   UseType = ReportPrepaidServiceCardViewModel.UseType.Sell,
                                   Date = bill.Date,
                                   Customer = bill.Customer,
                                   BillId = bill.Id,
                                   PrepaidServiceCard = card,
                                   Service = card.Service,
                                   Note = bill.Note,
                                   Quantity = card.Quota,
                                   Price = card.SellMoney / card.Quota,
                                   Discount = billItem.Discount ?? 0,
                                   Money = card.SellMoney
                               });
            }

            #endregion

            #region use card

            var histories = PrepaidCardHistoryRepository.AllEntities.Where(x => x.Date >= filter.StartDate && x.Date <= filter.EndDate).OfType<PrepaidServiceCardHistory>();
            lines.AddRange(from history in histories.ToList()
                           let card = history.PrepaidServiceCard
                           select new ReportPrepaidServiceCardViewModel.ReportLine
                           {
                               UseType = ReportPrepaidServiceCardViewModel.UseType.Use,
                               Date = history.Date,
                               Customer = card.Customer,
                               PrepaidServiceCard = card,
                               Service = card.Service,
                               Note = history.Note,
                               Quantity = history.UsedAmount,
                               Price = card.SellMoney / card.Quota,
                               Discount = 0,
                               Money = history.UsedAmount * card.SellMoney / card.Quota,
                               UsedProducts = history.UsedProducts,
                               Performers = history.Performers
                           });

            #endregion

            ret.Lines = lines.OrderByDescending(x => x.Date);
            return ret;
        }
        public byte[] GetExcelPrepaidServiceCard(ReportPrepaidServiceCardViewModel model)
        {
            var excel = new ExcelHelper("Phiếu dịch vụ");

            excel.SetValue("B2", "Báo cáo phiếu dịch vụ");
            excel.SetDisplayStyle("B2", new ExcelHelper.CellStyle { FontSize = 16, FontStyle = ExcelHelper.FontStyle.Bold, FontColor = Color.Red });

            excel.SetValue("C3", model.FilterModel.StartDate);
            excel.SetValue("B3", "Từ ngày");
            excel.SetDisplayStyle("B3", new ExcelHelper.CellStyle { FontStyle = ExcelHelper.FontStyle.Bold });

            excel.SetValue("C4", model.FilterModel.EndDate);
            excel.SetValue("B4", "Đến ngày");
            excel.SetDisplayStyle("B4", new ExcelHelper.CellStyle { FontStyle = ExcelHelper.FontStyle.Bold });

            var headers = new string[] { "Ngày", "Hóa đơn", "Loại phiếu", "Phiếu", "Khóa", "Hình thức", "Khách hàng", "Mã khách hàng", "Dịch vụ", "Mã dịch vụ", "Nhân viên thực hiện", "Sản phẩm dùng kèm", "Ghi chú", "Số suất", "Đơn giá", "Giảm giá", "Thành tiền" };
            excel.SetValuesInRow("B6", headers);

            const int startRow = 7;
            //const int startCol = 2;

            var row = startRow;
            foreach (var line in model.Lines)
            {
                excel.SetValuesInRow("B" + row,
                    line.Date,
                    line.BillId,
                    line.PrepaidServiceCard != null ? line.PrepaidServiceCard.CardType.Name : string.Empty,
                    line.PrepaidServiceCard != null ? line.PrepaidServiceCard.SerialNumber : string.Empty,
                    line.PrepaidServiceCard != null && line.PrepaidServiceCard.IsLocked ? "Đã khóa" : string.Empty,
                    line.UseType == ReportPrepaidServiceCardViewModel.UseType.Import ? "Nhập" : line.UseType == ReportPrepaidServiceCardViewModel.UseType.Sell ? "Bán" : "Sử dụng",
                    GetCustomerName(line.Customer),
                    (line.Customer != null ? line.Customer.Id : 0),
                    line.Service.Name,
                    line.Service.Id,
                    string.Join(", ", (line.Performers ?? Enumerable.Empty<User>()).Select(x => x.DisplayName)),
                    string.Join(", ", (line.UsedProducts ?? Enumerable.Empty<ProductsUsedInService>()).Select(x => x.Product.Name + " (" + x.Quantity + ")")),
                    line.Note,
                    line.Quantity,
                    line.Price,
                    line.Discount,
                    line.Money
                );
                row++;
            }

            return excel.GetContent();
        }

        public ReportDebtViewModel GetReportDebt(ReportFilterModel filter)
        {
            var ret = new ReportDebtViewModel { FilterModel = filter };

            var lines = new List<ReportDebtViewModel.ReportLine>();

            #region create debt

            var bills = BillRepository.AllEntities.Where(x => x.Date >= filter.StartDate && x.Date <= filter.EndDate && x.Debt != null);
            foreach (var bill in bills.ToList())
            {
                if (filter.CustomerIds != null && !filter.CustomerIds.Contains(bill.Customer.Id))
                    continue;

                var line = new ReportDebtViewModel.ReportLine
                {
                    BillId = bill.Id,
                    Customer = bill.Customer,
                    Date = bill.Date,
                    IsPayDebt = false,
                    Money = -bill.Debt.Money,
                    Note = bill.Note
                };
                lines.Add(line);
            }

            #endregion

            #region pay debt

            var histories = DebtHistoryRepository.AllEntities.Where(x => x.Date >= filter.StartDate && x.Date <= filter.EndDate);
            foreach (var history in histories.ToList())
            {
                if (filter.CustomerIds != null && !filter.CustomerIds.Contains(history.Customer.Id))
                    continue;

                var line = new ReportDebtViewModel.ReportLine
                {
                    DebtHistoryId = history.Id,
                    Customer = history.Customer,
                    Date = history.Date,
                    IsPayDebt = true,
                    Money = history.Money,
                    Note = history.Note
                };
                lines.Add(line);
            }

            #endregion

            ret.Lines = lines.OrderByDescending(x => x.Date);

            return ret;
        }
        public byte[] GetExcelDebt(ReportDebtViewModel model)
        {
            var excel = new ExcelHelper("Nợ");

            excel.SetValue("B2", "Báo cáo nợ");
            excel.SetDisplayStyle("B2", new ExcelHelper.CellStyle { FontSize = 16, FontStyle = ExcelHelper.FontStyle.Bold, FontColor = Color.Red });

            excel.SetValue("C3", model.FilterModel.StartDate);
            excel.SetValue("B3", "Từ ngày");
            excel.SetDisplayStyle("B3", new ExcelHelper.CellStyle { FontStyle = ExcelHelper.FontStyle.Bold });

            excel.SetValue("C4", model.FilterModel.EndDate);
            excel.SetValue("B4", "Đến ngày");
            excel.SetDisplayStyle("B4", new ExcelHelper.CellStyle { FontStyle = ExcelHelper.FontStyle.Bold });

            var headers = new string[] { "Ngày", "Hóa đơn", "Hình thức", "Khách hàng", "Mã khách hàng", "Số tiền", "Ghi chú" };
            excel.SetValuesInRow("B6", headers);

            const int startRow = 7;
            //const int startCol = 2;

            var row = startRow;
            foreach (var line in model.Lines)
            {
                excel.SetValuesInRow("B" + row,
                    line.Date,
                    line.BillId,
                    line.IsPayDebt ? "Trả nợ" : "Nợ",
                    GetCustomerName(line.Customer),
                    (line.Customer != null ? line.Customer.Id : 0),
                    line.Money,
                    line.Note
                );
                row++;
            }

            return excel.GetContent();
        }

        private string GetCustomerName(Customer customer)
        {
            var ret = string.Empty;
            if (customer != null)
                ret = customer.Name + (customer.IsVip ? " [VIP]" : string.Empty);
            return ret;
        }
    }
}
