﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using EVASPA.Core.Repository;
using EVASPA.Core.Service;
using EVASPA.Core.UnitOfWork;
using EVASPA.Model.DataModel;
using System;
using System.Linq;

namespace EVASPA.Service
{
    public class PrepaidCardService : IPrepaidCardService
    {
        private const string SerialChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        private static readonly Random Random = new Random(DateTime.Now.GetHashCode());

        private IPrepaidCardRepository PrepaidCardRepository { get; set; }
        private IPrepaidCardHistoryRepository PrepaidCardHistoryRepository { get; set; }
        private IItemRepository ItemRepository { get; set; }
        private IUserRepository UserRepository { get; set; }

        private IProductsUsedInServiceRepository ProductsUsedInServiceRepository { get; set; }
        private IUnitOfWork UnitOfWork { get; set; }
        private CommonService<PrepaidCard> CommonService { get; set; }

        public PrepaidCardService(IUnitOfWork unitOfWork, IPrepaidCardRepository prepaidCardRepository, IPrepaidCardHistoryRepository prepaidCardHistoryRepository, IItemRepository itemRepository, IUserRepository userRepository, IProductsUsedInServiceRepository productsUsedInServiceRepository)
        {
            UnitOfWork = unitOfWork;
            PrepaidCardRepository = prepaidCardRepository;
            PrepaidCardHistoryRepository = prepaidCardHistoryRepository;
            ItemRepository = itemRepository;
            UserRepository = userRepository;
            ProductsUsedInServiceRepository = productsUsedInServiceRepository;
            CommonService = new CommonService<PrepaidCard>(UnitOfWork, PrepaidCardRepository);
        }

        public IQueryable<PrepaidCard> AllEntities
        {
            get { return CommonService.AllEntities; }
        }

        public IQueryable<PrepaidCard> AllEntitiesIncludedDeleted
        {
            get { return CommonService.AllEntitiesIncludedDeleted; }
        }

        public PrepaidCard GetById(int? entityId, bool includeDeleted = false)
        {
            return CommonService.GetById(entityId, includeDeleted);
        }

        public PrepaidCard Add(PrepaidCard entity)
        {
            return CommonService.Add(entity);
        }

        public PrepaidCard Update(PrepaidCard entity)
        {
            return CommonService.Update(entity);
        }

        public void Delete(PrepaidCard entity)
        {
            CommonService.Delete(entity);
        }

        public void Delete(int entityId)
        {
            CommonService.Delete(entityId);
        }

        public IQueryable<PrepaidMoneyCard> AllMoneyCards
        {
            get { return AllEntities.OfType<PrepaidMoneyCard>(); }
        }

        public decimal GetCardAvailable(PrepaidCard card)
        {
            var ret = 0M;
            if (card is PrepaidMoneyCard)
            {
                var moneycard = card as PrepaidMoneyCard;
                ret = moneycard.Quota - moneycard.History.Where(x => !x.IsDeleted).Sum(x => x.UsedAmount);
            }
            else if (card is PrepaidServiceCard)
            {
                var servicecard = card as PrepaidServiceCard;
                ret = servicecard.Quota - servicecard.History.Where(x => !x.IsDeleted).Sum(x => x.UsedAmount);
            }
            return ret;
        }

        public string GenerateNewCardSerialNo()
        {
            string id;
            do
            {
                id = string.Empty;
                for (var i = 0; i < 6; i++)
                {
                    id += SerialChars[Random.Next(0, SerialChars.Length)];
                }
            } while (
                PrepaidCardRepository.AllEntitiesIncludedDeleted.Any(
                    x => string.Compare(x.SerialNumber, id, StringComparison.OrdinalIgnoreCase) == 0));

            return id;
        }

        public IQueryable<PrepaidCard> AllCardsWithPreLoadedInfo
        {
            get
            {
                return PrepaidCardRepository.AllPrepaidCardsWithPreLoadedInfo;
            }
        }

        public PrepaidCard GetBySerialNumber(string serialNumber)
        {
            var trim = serialNumber.Trim();
            return
                PrepaidCardRepository.FirstOrDefault(
                    x => string.Compare(x.SerialNumber, trim, StringComparison.OrdinalIgnoreCase) == 0);
        }

        public void UseServiceCard(PrepaidServiceCardHistory history, int[] performers)
        {
            // check quota
            var serviceCard = PrepaidCardRepository.FirstOrDefault(x => x.Id == history.PrepaidServiceCardId) as PrepaidServiceCard;
            var available = GetCardAvailable(serviceCard);
            if (available < history.UsedAmount)
            {
                throw new Exception("Số lượng vượt quá hạn mức còn lại của phiếu (" + available + ").");
            }

            if (history.UsedProducts != null)
            {
                history.UsedProducts = history.UsedProducts.Where(x => x.Quantity > 0).ToList();
            }

            if (performers != null && performers.Any())
            {
                history.Performers = UserRepository.Attach(performers);
            }
            else
            {
                throw new Exception("Chưa chọn người thực hiện.");
            }

            if (history.UsedProducts != null)
            {
                foreach (var usedProduct in history.UsedProducts)
                {
                    var item = ItemRepository.FirstOrDefault(x => x.Id == usedProduct.ProductId);
                    if (item.QuantityInStock >= usedProduct.Quantity)
                    {
                        item.QuantityInStock -= usedProduct.Quantity;
                    }
                    else
                    {
                        throw new Exception(string.Format("Không đủ sản phẩm {0} trong kho (số lượng: {1})", item.Name,
                            item.QuantityInStock));
                    }
                }
            }

            PrepaidCardHistoryRepository.Add(history);
            UnitOfWork.SaveChanges();
        }

        public void LockCard(string serialNumber)
        {
            var card = GetBySerialNumber(serialNumber);
            if (card != null)
            {
                card.IsLocked = !card.IsLocked;
                UnitOfWork.SaveChanges();
            }
        }

        public int DeleteHistory(int id)
        {
            var cardHistory = PrepaidCardHistoryRepository.FirstOrDefault(x => x.Id == id);
            if (cardHistory == null || !(cardHistory is PrepaidServiceCardHistory))
                return -1;

            PrepaidCardHistoryRepository.Delete(cardHistory);
            var svcCard = cardHistory as PrepaidServiceCardHistory;
            svcCard.Performers = new List<User>();
            foreach (var prod in svcCard.UsedProducts)
            {
                prod.Product.QuantityInStock += prod.Quantity;
                ProductsUsedInServiceRepository.Delete(prod);
            }

            UnitOfWork.SaveChanges();
            return svcCard.PrepaidServiceCard.Id;
        }
    }
}