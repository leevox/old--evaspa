﻿using EVASPA.Core.Repository;
using EVASPA.Core.Service;
using EVASPA.Core.UnitOfWork;
using System.Linq;
using EVASPA.Model.DataModel;

namespace EVASPA.Service
{
    public class ItemHistoryService : IItemHistoryService
    {
        private IItemRepository ItemRepository { get; set; }
        private IItemHistoryRepository ItemHistoryRepository { get; set; }
        private IUnitOfWork UnitOfWork { get; set; }
        private CommonService<ItemHistory> CommonService { get; set; }

        public ItemHistoryService(IUnitOfWork unitOfWork, IItemHistoryRepository itemHistoryRepository, IItemRepository itemRepository)
        {
            UnitOfWork = unitOfWork;
            ItemHistoryRepository = itemHistoryRepository;
            ItemRepository = itemRepository;
            CommonService = new CommonService<ItemHistory>(UnitOfWork, ItemHistoryRepository);
        }

        public IQueryable<ItemHistory> AllEntities
        {
            get { return CommonService.AllEntities; }
        }

        public IQueryable<ItemHistory> AllEntitiesIncludedDeleted
        {
            get { return CommonService.AllEntitiesIncludedDeleted; }
        }

        public ItemHistory GetById(int? entityId, bool includeDeleted = false)
        {
            return CommonService.GetById(entityId, includeDeleted);
        }

        public ItemHistory Add(ItemHistory entity)
        {
            return CommonService.Add(entity);
        }

        public ItemHistory Update(ItemHistory entity)
        {
            return CommonService.Update(entity);
        }

        public void Delete(ItemHistory entity)
        {
            if (entity.IsDeleted)
                return;

            entity.Item.QuantityInStock -= entity.Quantity;
            ItemRepository.Update(entity.Item);
            CommonService.Delete(entity);
        }

        public void Delete(int entityId)
        {
            Delete(GetById(entityId));
        }
    }
}