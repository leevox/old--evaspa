﻿using EVASPA.Core.Repository;
using EVASPA.Core.Service;
using EVASPA.Core.UnitOfWork;
using System.Linq;
using EVASPA.Model.DataModel;

namespace EVASPA.Service
{
    public class DebtHistoryService : IDebtHistoryService
    {
        private IDebtHistoryRepository DebtHistoryRepository { get; set; }
        private IUnitOfWork UnitOfWork { get; set; }
        private CommonService<DebtHistory> CommonService { get; set; }

        public DebtHistoryService(IUnitOfWork unitOfWork, IDebtHistoryRepository debtHistoryRepository)
        {
            UnitOfWork = unitOfWork;
            DebtHistoryRepository = debtHistoryRepository;
            CommonService = new CommonService<DebtHistory>(UnitOfWork, DebtHistoryRepository);
        }

        public IQueryable<DebtHistory> AllEntities
        {
            get { return CommonService.AllEntities; }
        }

        public IQueryable<DebtHistory> AllEntitiesIncludedDeleted
        {
            get { return CommonService.AllEntitiesIncludedDeleted; }
        }

        public DebtHistory GetById(int? entityId, bool includeDeleted = false)
        {
            return CommonService.GetById(entityId, includeDeleted);
        }

        public DebtHistory Add(DebtHistory entity)
        {
            return CommonService.Add(entity);
        }

        public DebtHistory Update(DebtHistory entity)
        {
            return CommonService.Update(entity);
        }

        public void Delete(DebtHistory entity)
        {
            CommonService.Delete(entity);
        }

        public void Delete(int entityId)
        {
            CommonService.Delete(entityId);
        }
    }
}