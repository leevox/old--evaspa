using EVASPA.Core.Repository;
using EVASPA.Core.Service;
using EVASPA.Core.UnitOfWork;
using System.Linq;
using EVASPA.Model.DataModel;

namespace EVASPA.Service
{
    public class LeavingService : ILeavingService
    {
        private ILeavingRepository LeavingRepository { get; set; }
        private IUnitOfWork UnitOfWork { get; set; }
        private CommonService<Leaving> CommonService { get; set; }

        public LeavingService(IUnitOfWork unitOfWork, ILeavingRepository leavingRepository)
        {
            UnitOfWork = unitOfWork;
            LeavingRepository = leavingRepository;
            CommonService = new CommonService<Leaving>(UnitOfWork, LeavingRepository);
        }

        public IQueryable<Leaving> AllEntities
        {
            get { return CommonService.AllEntities; }
        }

        public IQueryable<Leaving> AllEntitiesIncludedDeleted
        {
            get { return CommonService.AllEntitiesIncludedDeleted; }
        }

        public Leaving GetById(int? entityId, bool includeDeleted = false)
        {
            return CommonService.GetById(entityId, includeDeleted);
        }

        public Leaving Add(Leaving entity)
        {
            return CommonService.Add(entity);
        }

        public Leaving Update(Leaving entity)
        {
            return CommonService.Update(entity);
        }

        public void Delete(Leaving entity)
        {
            CommonService.Delete(entity);
        }

        public void Delete(int entityId)
        {
            CommonService.Delete(entityId);
        }
    }
}