﻿using EVASPA.Core.Repository;
using EVASPA.Core.Service;
using EVASPA.Core.UnitOfWork;
using EVASPA.Model.DataModel;
using System;
using System.Linq;

namespace EVASPA.Service
{
    public class ItemService : IItemService
    {
        private IUnitOfWork UnitOfWork { get; set; }
        private IItemRepository ItemRepository { get; set; }
        private IItemHistoryRepository ItemHistoryRepository { get; set; }
        private CommonService<Item> CommonService { get; set; }

        public ItemService(IUnitOfWork unitOfWork, IItemRepository itemRepository, IItemHistoryRepository itemHistoryRepository)
        {
            UnitOfWork = unitOfWork;
            ItemRepository = itemRepository;
            ItemHistoryRepository = itemHistoryRepository;
            CommonService = new CommonService<Item>(UnitOfWork, ItemRepository);
        }

        public IQueryable<Item> AllEntities
        {
            get { return CommonService.AllEntities; }
        }

        public IQueryable<Item> AllEntitiesIncludedDeleted
        {
            get { return CommonService.AllEntitiesIncludedDeleted; }
        }

        public Item GetById(int? entityId, bool includeDeleted = false)
        {
            return CommonService.GetById(entityId, includeDeleted);
        }

        public Item Add(Item entity)
        {
            var isExisted =
                ItemRepository.Any(
                    x => String.Compare(x.Name, entity.Name, StringComparison.CurrentCultureIgnoreCase) == 0);

            if (isExisted)
            {
                throw new Exception("Sản phẩm đã tồn tại.");
            }

            return CommonService.Add(entity);
        }

        public Item Update(Item entity)
        {
            return CommonService.Update(entity);
        }

        public void Delete(Item entity)
        {
            CommonService.Delete(entity);
        }

        public void Delete(int entityId)
        {
            CommonService.Delete(entityId);
        }

        public IQueryable<Item> GetAllItemsWhoseQuantityIsEqualOrLowerThan(int limit)
        {
            return
                ItemRepository.Where(
                    x => !x.Discontinued && x.QuantityInStock <= limit && !(x is Model.DataModel.Service));
        }

        public IQueryable<Product> GetAllProducts()
        {
            return ItemRepository.AllProducts.OrderBy(x => x.Discontinued).ThenBy(x => x.Name);
        }

        public Product GetProductById(int? id)
        {
            return ItemRepository.AllProducts.FirstOrDefault(x => x.Id == id);
        }

        public IQueryable<Model.DataModel.Service> GetAllServices()
        {
            return ItemRepository.AllServices.OrderBy(x => x.Discontinued).ThenBy(x => x.Name);
        }

        public Model.DataModel.Service GetServiceById(int? id)
        {
            return ItemRepository.AllServices.FirstOrDefault(x => x.Id == id);
        }

        public IQueryable<CardType> GetAllCardTypes()
        {
            return ItemRepository.AllCardTypes.OrderBy(x => x.Discontinued).ThenBy(x => x.Name);
        }

        public CardType GetCardTypeById(int? id)
        {
            return ItemRepository.AllCardTypes.FirstOrDefault(x => x.Id == id);
        }

        public void Import(ItemHistory model)
        {
            var item = ItemRepository.FirstOrDefault(x => x.Id == model.ItemId && (x is Product || x is CardType));
            if (item != null)
            {
                item.QuantityInStock += model.Quantity;
                ItemHistoryRepository.Add(model);
            }

            UnitOfWork.SaveChanges();
        }
    }
}