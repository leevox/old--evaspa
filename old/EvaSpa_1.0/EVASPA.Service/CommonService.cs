﻿using EVASPA.Core.Repository;
using EVASPA.Core.UnitOfWork;
using System.Linq;
using EVASPA.Model.DataModel;

namespace EVASPA.Service
{
    public class CommonService<TEntity> where TEntity : EntityBase 
    {
        IUnitOfWork UnitOfWork { get; set; }
        IRepository<TEntity> Repository { get; set; }

        public CommonService(IUnitOfWork unitOfWork, IRepository<TEntity> repository)
        {
            UnitOfWork = unitOfWork;
            Repository = repository;
        }

        public IQueryable<TEntity> AllEntities
        {
            get { return Repository.AllEntities; }
        }

        public IQueryable<TEntity> AllEntitiesIncludedDeleted
        {
            get { return Repository.AllEntitiesIncludedDeleted; }
        }

        public TEntity GetById(int? entityId, bool includeDeleted = false)
        {
            return (includeDeleted ? AllEntitiesIncludedDeleted : AllEntities).FirstOrDefault(x => x.Id == entityId);
        }

        public TEntity Add(TEntity entity)
        {
            Repository.Add(entity);
            UnitOfWork.SaveChanges();
            return entity;
        }

        public TEntity Update(TEntity entity)
        {
            Repository.Update(entity);
            UnitOfWork.SaveChanges();
            return entity;
        }

        public void Delete(TEntity entity)
        {
            Repository.Delete(entity);
            UnitOfWork.SaveChanges();
        }

        public void Delete(int entityId)
        {
            var entity = GetById(entityId);
            if (entity != null && !entity.IsDeleted)
            {
                Delete(entity);
            }
        }
    }
}
