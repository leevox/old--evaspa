﻿using EVASPA.Core.Repository;
using EVASPA.Core.Service;
using EVASPA.Core.UnitOfWork;
using System.Linq;
using EVASPA.Model.DataModel;

namespace EVASPA.Service
{
    public class BillItemService : IBillItemService
    {
        private IBillItemRepository BillItemRepository { get; set; }
        private IUnitOfWork UnitOfWork { get; set; }
        private CommonService<BillItem> CommonService { get; set; }

        public BillItemService(IUnitOfWork unitOfWork, IBillItemRepository billItemRepository)
        {
            UnitOfWork = unitOfWork;
            BillItemRepository = billItemRepository;
            CommonService = new CommonService<BillItem>(UnitOfWork, BillItemRepository);
        }

        public IQueryable<BillItem> AllEntities
        {
            get { return CommonService.AllEntities; }
        }

        public IQueryable<BillItem> AllEntitiesIncludedDeleted
        {
            get { return CommonService.AllEntitiesIncludedDeleted; }
        }

        public BillItem GetById(int? entityId, bool includeDeleted = false)
        {
            return CommonService.GetById(entityId, includeDeleted);
        }

        public BillItem Add(BillItem entity)
        {
            return CommonService.Add(entity);
        }

        public BillItem Update(BillItem entity)
        {
            return CommonService.Update(entity);
        }

        public void Delete(BillItem entity)
        {
            CommonService.Delete(entity);
        }

        public void Delete(int entityId)
        {
            CommonService.Delete(entityId);
        }
    }
}