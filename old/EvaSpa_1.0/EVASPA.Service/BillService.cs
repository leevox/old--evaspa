﻿using EVASPA.Core.Repository;
using EVASPA.Core.Service;
using EVASPA.Core.UnitOfWork;
using EVASPA.Model.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using EVASPA.Model.ViewModel;

namespace EVASPA.Service
{
    public class BillService : IBillService
    {
        private IBillRepository BillRepository { get; set; }

        private IBillItemRepository BillItemRepository { get; set; }

        private IPrepaidCardRepository PrepaidCardRepository { get; set; }

        private IPrepaidCardHistoryRepository PrepaidCardHistoryRepository { get; set; }

        private IUserRepository UserRepository { get; set; }

        private IItemRepository ItemRepository { get; set; }
        
        private IPrepaidCardService PrepaidCardService { get; set; }

        private IDebtRepository DebtRepository { get; set; }

        private IProductsUsedInServiceRepository ProductsUsedInServiceRepository { get; set; }

        private IUnitOfWork UnitOfWork { get; set; }

        private CommonService<Bill> CommonService { get; set; }

        public BillService(IUnitOfWork unitOfWork, IBillRepository billRepository, IBillItemRepository billItemRepository, IPrepaidCardRepository prepaidCardRepository, IPrepaidCardHistoryRepository prepaidCardHistoryRepository, IUserRepository userRepository, IItemRepository itemRepository, IPrepaidCardService prepaidCardService, IDebtRepository debtRepository, IProductsUsedInServiceRepository productsUsedInServiceRepository)
        {
            BillRepository = billRepository;
            BillItemRepository = billItemRepository;
            PrepaidCardRepository = prepaidCardRepository;
            PrepaidCardHistoryRepository = prepaidCardHistoryRepository;
            UserRepository = userRepository;
            ItemRepository = itemRepository;
            DebtRepository = debtRepository;
            ProductsUsedInServiceRepository = productsUsedInServiceRepository;

            PrepaidCardService = prepaidCardService;

            UnitOfWork = unitOfWork;
            CommonService = new CommonService<Bill>(UnitOfWork, BillRepository);
        }

        public IQueryable<Bill> AllEntities
        {
            get { return CommonService.AllEntities; }
        }

        public IQueryable<Bill> AllEntitiesIncludedDeleted
        {
            get { return CommonService.AllEntitiesIncludedDeleted; }
        }

        public Bill GetById(int? entityId, bool includeDeleted = false)
        {
            return CommonService.GetById(entityId, includeDeleted);
        }

        public Bill Add(Bill entity)
        {
            return CommonService.Add(entity);
        }

        public Bill Update(Bill entity)
        {
            return CommonService.Update(entity);
        }

        public void Delete(int entityId)
        {
            Delete(AllEntities.FirstOrDefault(x=>x.Id == entityId));
        }

        public void Delete(Bill entity)
        {
            BillRepository.Delete(entity);
            foreach(var billItem in entity.BillItems)
            {
                BillItemRepository.Delete(billItem);

                if (billItem is BillItemProduct)
                {
                    (billItem as BillItemProduct).Product.QuantityInStock += billItem.Quantity;
                }
                else if (billItem is EVASPA.Model.DataModel.BillItemService)
                {
                    var svcItem = billItem as EVASPA.Model.DataModel.BillItemService;
                    if (svcItem.UsedProducts != null)
                    {
                        foreach(var productUseInSvc in svcItem.UsedProducts)
                        {
                            ProductsUsedInServiceRepository.Delete(productUseInSvc);
                            productUseInSvc.Product.QuantityInStock += productUseInSvc.Quantity;
                        }
                    }
                    if (svcItem.Performers != null)
                    {
                        svcItem.Performers.Clear();
                    }
                }
                else if(billItem is BillItemCard)
                {
                    var card = (billItem as BillItemCard).PrepaidCard;
                    card.CardType.QuantityInStock += 1;
                    PrepaidCardRepository.Delete(card);

                    if (card is PrepaidMoneyCard)
                    {
                        var moneyCard = card as PrepaidMoneyCard;
                        if (moneyCard.History != null)
                        {
                            foreach (var history in moneyCard.History)
                            {
                                if (history.UsedInBill.Debt == null)
                                {
                                    history.UsedInBill.Debt = new Debt
                                    {
                                        CustomerId = history.UsedInBill.Customer.Id,
                                        Money = history.UsedAmount
                                    };
                                }
                                else
                                {
                                    history.UsedInBill.Debt.Money += history.UsedAmount;
                                }
                            }
                        }
                    }
                    else if(card is PrepaidServiceCard)
                    {
                        var svcCard = card as PrepaidServiceCard;
                        foreach(var history in svcCard.History)
                        {
                            PrepaidCardHistoryRepository.Delete(history);

                            history.Performers = new List<User>();
                            if(history.UsedProducts != null)
                            {
                                foreach(var usedProduct in history.UsedProducts)
                                {
                                    usedProduct.Product.QuantityInStock += usedProduct.Quantity;
                                    ProductsUsedInServiceRepository.Delete(usedProduct);
                                }
                            }
                        }
                    }
                }
            }

            if (entity.UsedCards != null)
            {
                foreach(var cardHistory in entity.UsedCards)
                {
                    PrepaidCardHistoryRepository.Delete(cardHistory);
                }
            }

            if (entity.Debt != null)
            {
                DebtRepository.Delete(entity.Debt);
            }

            UnitOfWork.SaveChanges();
        }

        public int NewBillId {
            get { return BillRepository.AllEntitiesIncludedDeleted.Max(x => x.Id) + 1; }
        }

        public Bill CreateBill(BillViewModel model)
        {
            var bill = new Bill
            {
                Date = DateTime.ParseExact(model.Date, "dd/MM/yyyy", null),
                CustomerId = model.CustomerId,
                SellerId = model.SellerId,
                CustomerMoney = model.CustomerMoney,
                Note = model.Note ?? string.Empty,
                BillItems = new List<BillItem>()
            };

            #region load all items

            var soldItemIds = new List<int>();
            if (model.PrepaidMoneyCards != null) model.PrepaidMoneyCards.ForEach(x => soldItemIds.Add(x.CardTypeId));
            if (model.PrepaidServiceCards != null) model.PrepaidServiceCards.ForEach(x => soldItemIds.Add(x.CardTypeId));
            if (model.Products != null) model.Products.ForEach(x => soldItemIds.Add(x.ProductId));
            if (model.Services != null) 
                model.Services.ForEach(x =>
                {
                    if (x.UsedProductIds != null && x.UsedProductIds.Any())
                    {
                        soldItemIds.AddRange(x.UsedProductIds);
                    }
                });

            var soldItems = ItemRepository.AllEntities.Where(x => soldItemIds.Contains(x.Id));
            var soldProducts = soldItems.OfType<Product>();
            var soldCardTypes = soldItems.OfType<CardType>();

            #endregion

            #region Bill Item Products

            if (model.Products != null && model.Products.Count > 0)
            {
                foreach (var sellItemViewModel in model.Products)
                {
                    var p = soldProducts.FirstOrDefault(x => x.Id == sellItemViewModel.ProductId);

                    if (p == null)
                    {
                        throw new Exception(string.Format("Mã sản phẩm không đúng: " + sellItemViewModel.ProductId));
                    }
                    
                    if (p.QuantityInStock < sellItemViewModel.Quantity)
                    {
                        throw new Exception(string.Format("Không đủ sản phẩm \"{0}\" để bán. (Trong kho: {1})", p.Name, p.QuantityInStock));
                    }

                    p.QuantityInStock -= sellItemViewModel.Quantity;

                    var billItem = new BillItemProduct
                    {
                        Bill = bill,
                        Price = sellItemViewModel.Price,
                        Quantity = sellItemViewModel.Quantity,
                        Discount = sellItemViewModel.Discount,
                        ProductId = sellItemViewModel.ProductId
                    };
                    bill.BillItems.Add(billItem);
                }
            }

            #endregion

            #region Bill Item Services

            if (model.Services != null && model.Services.Count > 0)
            {
                foreach (var billItemViewModel in model.Services)
                {
                    var billItem = new Model.DataModel.BillItemService
                    {
                        Bill = bill,
                        Price = billItemViewModel.Price,
                        Quantity = billItemViewModel.Quantity,
                        Discount = billItemViewModel.Discount,
                        ServiceId = billItemViewModel.ServiceId,
                        Performers = UserRepository.Attach(billItemViewModel.PerformerIds.ToArray()),
                        UsedProducts = new List<ProductsUsedInService>()
                    };

                    if (billItemViewModel.UsedProductIds != null && billItemViewModel.UsedProductIds.Any())
                    {
                        for (var i = 0; i < billItemViewModel.UsedProductIds.Count; i++)
                        {
                            var id = billItemViewModel.UsedProductIds[i];
                            var p = soldProducts.FirstOrDefault(x => x.Id == id);

                            if (p == null)
                            {
                                throw new Exception(string.Format("Mã sản phẩm không đúng: " + billItemViewModel.UsedProductIds[i]));
                            }

                            if (p.QuantityInStock < billItemViewModel.UsedProductCounts[i])
                            {
                                throw new Exception(string.Format("Không đủ sản phẩm \"{0}\" để sử dụng dịch vụ. (Trong kho: {1})", p.Name,  p.QuantityInStock));
                            }

                            p.QuantityInStock -= billItemViewModel.UsedProductCounts[i];

                            billItem.UsedProducts.Add(new ProductsUsedInService
                            {
                                ProductId = billItemViewModel.UsedProductIds[i],
                                Quantity = billItemViewModel.UsedProductCounts[i]
                            });
                        }    
                    }
                    
                    bill.BillItems.Add(billItem);
                }
            }

            #endregion

            #region Bill Item Money Cards

            if (model.PrepaidMoneyCards != null && model.PrepaidMoneyCards.Count > 0)
            {
                foreach (var sellItemViewModel in model.PrepaidMoneyCards)
                {
                    var p = soldCardTypes.FirstOrDefault(x => x.Id == sellItemViewModel.CardTypeId);

                    if (p == null)
                    {
                        throw new Exception(string.Format("Mã loại phiếu không tồn tại: " + sellItemViewModel.CardTypeId));
                    }

                    if (p.QuantityInStock < 1)
                    {
                        throw new Exception(string.Format("Không đủ loại phiếu \"{0}\" để bán. (Trong kho: {1})", p.Name, p.QuantityInStock));
                    }

                    p.QuantityInStock -= 1;

                    var card = new PrepaidMoneyCard
                    {
                        CardTypeId = sellItemViewModel.CardTypeId,
                        CustomerId = model.CustomerId,
                        SerialNumber = PrepaidCardService.GenerateNewCardSerialNo(),
                        SoldDate = bill.Date,
                        Quota = sellItemViewModel.Quota,
                        SellMoney = sellItemViewModel.Quota - (sellItemViewModel.Discount ?? 0)
                    };
                    var billItem = new BillItemCard
                    {
                        Bill = bill,
                        Price = sellItemViewModel.Quota,
                        Quantity = 1,
                        Discount = sellItemViewModel.Discount,
                        PrepaidCard = card
                    };
                    bill.BillItems.Add(billItem);
                }
            }

            #endregion

            #region Bill Item Service Cards

            if (model.PrepaidServiceCards != null && model.PrepaidServiceCards.Count > 0)
            {
                foreach (var sellItemViewModel in model.PrepaidServiceCards)
                {
                    var p = soldCardTypes.FirstOrDefault(x => x.Id == sellItemViewModel.CardTypeId);

                    if (p == null)
                    {
                        throw new Exception(string.Format("Mã loại phiếu không tồn tại: " + sellItemViewModel.CardTypeId));
                    }

                    if (p.QuantityInStock < 1)
                    {
                        throw new Exception(string.Format("Không đủ loại phiếu \"{0}\" để bán. (Trong kho: {1})", p.Name, p.QuantityInStock));
                    }

                    p.QuantityInStock -= 1;

                    var card = new PrepaidServiceCard
                    {
                        CardTypeId = sellItemViewModel.CardTypeId,
                        CustomerId = model.CustomerId,
                        SerialNumber = PrepaidCardService.GenerateNewCardSerialNo(),
                        SoldDate = bill.Date,
                        Quota = sellItemViewModel.Quota,
                        ServiceId = sellItemViewModel.ServiceId,
                        SellMoney = sellItemViewModel.Price*sellItemViewModel.Quota - (sellItemViewModel.Discount ?? 0)
                    };
                    var billItem = new BillItemCard
                    {
                        Bill = bill,
                        Price = sellItemViewModel.Price,
                        Quantity = sellItemViewModel.Quota,
                        Discount = sellItemViewModel.Discount,
                        PrepaidCard = card
                    };
                    bill.BillItems.Add(billItem);
                }
            }

            #endregion

            var billMoney = bill.BillItems.Sum(x => x.FinalMoney);
            var cardMoney = 0M;

            #region Used Money Cards in bill

            if (model.UsedMoneyCards != null && model.UsedMoneyCards.Count > 0)
            {
                bill.UsedCards = new List<PrepaidMoneyCardHistory>();

                var allCards = PrepaidCardRepository.AllPrepaidMoneyCardsWithHistories
                        .Where(x => model.UsedMoneyCards.Contains(x.Id));
                var sortedCards = allCards.ToList().Select(x => new { card = x, available = x.Quota - x.History.Where(y => !y.IsDeleted).Sum(y => y.UsedAmount) })
                        .ToList().OrderByDescending(x => x.available);

                foreach (var card in sortedCards)
                {
                    if (card.available <= 0)
                    {
                        throw new Exception(string.Format("Phiếu \"{0}\" đã hết hạn mức sử dụng.", card.card.SerialNumber));
                    }

                    var usedAmount = Math.Min(card.available, billMoney - cardMoney);
                    bill.UsedCards.Add(new PrepaidMoneyCardHistory
                    {
                        UsedInBill = bill,
                        PrepaidMoneyCard = card.card,
                        UsedAmount = usedAmount,
                        Date = bill.Date
                    });
                    cardMoney += usedAmount;

                    if (cardMoney >= billMoney)
                        break;
                }
            }

            #endregion

            if (model.CustomerMoney + cardMoney < billMoney)
            {
                var debt = new Debt
                {
                    CustomerId = bill.CustomerId,
                    Money = billMoney - model.CustomerMoney - cardMoney
                };
                bill.Debt = debt;
            }

            return Add(bill);
        }
    }
}