﻿using EVASPA.Model.DataModel;

namespace EVASPA.Core.Repository
{
    public interface ILeavingRepository : IRepository<Leaving>
    {

    }
}