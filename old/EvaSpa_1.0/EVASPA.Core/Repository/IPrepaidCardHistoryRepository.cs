﻿using EVASPA.Model.DataModel;

namespace EVASPA.Core.Repository
{
    public interface IPrepaidCardHistoryRepository : IRepository<PrepaidCardHistory>
    {

    }
}