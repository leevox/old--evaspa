﻿using EVASPA.Model.DataModel;

namespace EVASPA.Core.Repository
{
    public interface IUserRepository : IRepository<User>
    {
    }
}
