﻿using System.Linq;
using EVASPA.Model.DataModel;

namespace EVASPA.Core.Repository
{
    public interface IItemRepository : IRepository<Item>
    {
        IQueryable<Product> AllProducts { get; }
        IQueryable<Model.DataModel.Service> AllServices { get; }
        IQueryable<CardType> AllCardTypes { get; }
    }
}