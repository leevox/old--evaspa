﻿using EVASPA.Model.DataModel;

namespace EVASPA.Core.Repository
{
    public interface IBillRepository : IRepository<Bill>
    {

    }
}