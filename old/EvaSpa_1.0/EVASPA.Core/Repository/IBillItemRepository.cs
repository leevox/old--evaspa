﻿using EVASPA.Model.DataModel;

namespace EVASPA.Core.Repository
{
    public interface IBillItemRepository : IRepository<BillItem>
    {

    }
}