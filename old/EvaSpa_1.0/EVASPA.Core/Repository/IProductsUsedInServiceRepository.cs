﻿using EVASPA.Model.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVASPA.Core.Repository
{
    public interface IProductsUsedInServiceRepository : IRepository<ProductsUsedInService>
    {
    }
}
