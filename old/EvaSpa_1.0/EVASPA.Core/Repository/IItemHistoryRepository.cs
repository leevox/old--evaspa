﻿using EVASPA.Model.DataModel;

namespace EVASPA.Core.Repository
{
    public interface IItemHistoryRepository : IRepository<ItemHistory>
    {

    }
}