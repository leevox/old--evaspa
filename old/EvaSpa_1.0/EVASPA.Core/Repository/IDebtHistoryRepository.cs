﻿using EVASPA.Model.DataModel;

namespace EVASPA.Core.Repository
{
    public interface IDebtHistoryRepository : IRepository<DebtHistory>
    {

    }
}