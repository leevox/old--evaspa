﻿using EVASPA.Model.DataModel;
using System.Linq;

namespace EVASPA.Core.Repository
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        IQueryable<Customer> AllCustomersWithDebt();
    }
}
