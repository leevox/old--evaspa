﻿using EVASPA.Model.DataModel;
using System.Linq;

namespace EVASPA.Core.Repository
{
    public interface IPrepaidCardRepository : IRepository<PrepaidCard>
    {
        IQueryable<PrepaidMoneyCard> AllPrepaidMoneyCards { get; }

        IQueryable<PrepaidMoneyCard> AllPrepaidMoneyCardsWithHistories { get; }

        IQueryable<PrepaidCard> AllPrepaidCardsWithPreLoadedInfo { get; }
    }
}