﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace EVASPA.Core.Repository
{
    public interface IRepository<TEntity> where TEntity : class
    {
        // All Entities which are not deleted
        IQueryable<TEntity> AllEntities { get; }

        // All Entities which are in any state (deleted or not deleted)
        IQueryable<TEntity> AllEntitiesIncludedDeleted { get; }
            
        TEntity FirstOrDefault();
        TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate);

        IQueryable<TEntity> Where(Expression<Func<TEntity, bool>> predicate);

        bool Any(Expression<Func<TEntity, bool>> predicate);

        TEntity Attach(int id);
        ICollection<TEntity> Attach(params int[] ids);

        TEntity Add(TEntity entity);
        TEntity Update(TEntity entity);
        void Delete(TEntity entity);
        void Delete(int? id);
    }
}
