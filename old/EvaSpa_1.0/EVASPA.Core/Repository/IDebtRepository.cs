﻿using EVASPA.Model.DataModel;

namespace EVASPA.Core.Repository
{
    public interface IDebtRepository : IRepository<Debt>
    {

    }
}