﻿using System.Collections.Generic;
using EVASPA.Core.UnitOfWork;
using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using EVASPA.Model.DataModel;

namespace EVASPA.Core.Repository
{
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : EntityBase
    {
        protected IUnitOfWork UnitOfWork;
        protected DbSet<TEntity> EntitySet;

        protected Repository(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;

            EntitySet = UnitOfWork.GetEntities<TEntity>();
        }

        public IQueryable<TEntity> AllEntities
        {
            get { return EntitySet.Where(x => !x.IsDeleted); }
        }

        public IQueryable<TEntity> AllEntitiesIncludedDeleted
        {
            get { return EntitySet; }
        }

        public bool Any(Expression<Func<TEntity, bool>> predicate)
        {
            return AllEntities.Any(predicate);
        }

        public TEntity FirstOrDefault()
        {
            return AllEntities.FirstOrDefault();
        }

        public TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return AllEntities.FirstOrDefault(predicate);
        }

        public IQueryable<TEntity> Where(Expression<Func<TEntity, bool>> predicate)
        {
            return AllEntities.Where(predicate);
        }

        public TEntity Attach(int id)
        {
            TEntity ret;

            var x = UnitOfWork.GetEntityInTracker<TEntity>(id);
            if (x == null)
            {
                ret = EntitySet.Create();
                ret.Id = id;
                EntitySet.Attach(ret);
            }
            else
            {
                ret = x;
            }

            return ret;
        }

        public ICollection<TEntity> Attach(params int[] ids)
        {
            var ret = new List<TEntity>();
            foreach (var id in ids)
                ret.Add(Attach(id));
            return ret;
        }

        public TEntity Add(TEntity entity)
        {
            EntitySet.Add(entity);
            return entity;
        }

        public TEntity Update(TEntity entity)
        {
            EntitySet.AddOrUpdate(entity);
            return entity;
        }

        public void Delete(TEntity entity)
        {
            entity.IsDeleted = true;
            Update(entity);
        }

        public void Delete(int? id)
        {
            Delete(FirstOrDefault(x => x.Id == id));
        }
    }
}
