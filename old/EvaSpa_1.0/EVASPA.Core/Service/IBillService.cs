using EVASPA.Model.DataModel;
using EVASPA.Model.ViewModel;

namespace EVASPA.Core.Service
{
    public interface IBillService : IService<Bill>
    {
        int NewBillId { get; }

        Bill CreateBill(BillViewModel viewModel);
    }
}