using EVASPA.Model.DataModel;

namespace EVASPA.Core.Service
{
    public interface IDebtService : IService<Debt>
    {
        void PayDebt(DebtHistory model);

        DebtHistory GetDebtHistoryById(int? id);

        void DeleteDebtHistory(int id);
    }
}