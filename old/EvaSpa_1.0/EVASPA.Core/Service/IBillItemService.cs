﻿using EVASPA.Model.DataModel;

namespace EVASPA.Core.Service
{
    public interface IBillItemService : IService<BillItem>
    {

    }
}