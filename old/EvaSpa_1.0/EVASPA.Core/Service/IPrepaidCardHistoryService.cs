﻿using EVASPA.Model.DataModel;

namespace EVASPA.Core.Service
{
    public interface IPrepaidCardHistoryService : IService<PrepaidCardHistory>
    {

    }
}