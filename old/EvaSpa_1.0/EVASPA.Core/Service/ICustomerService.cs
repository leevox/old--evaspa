﻿using EVASPA.Model.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EVASPA.Core.Service
{
    public interface ICustomerService : IService<Customer>
    {
        IQueryable<Customer> GetAllCustomerWhoHasBirthdayIsInDay(DateTime day);
        IQueryable<Customer> GetAllCustomerWhoHasBirthdayIsInRange(DateTime from, DateTime to);

        IQueryable<Customer> AllCustomersWithDebts();

        decimal GetCurrentDebt(Customer customer);
    }
}
