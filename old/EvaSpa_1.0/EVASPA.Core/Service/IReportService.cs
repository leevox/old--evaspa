﻿using EVASPA.Model.ViewModel;

namespace EVASPA.Core.Service
{
    public interface IReportService
    {
        ReportEmployeeViewModel GetReportEmployee(ReportFilterModel filter);
        byte[] GetExcelEmployee(ReportEmployeeViewModel model);

        ReportProductViewModel GetReportProduct(ReportFilterModel filter);
        byte[] GetExcelProduct(ReportProductViewModel model);

        ReportServiceViewModel GetReportService(ReportFilterModel filter);
        byte[] GetExcelService(ReportServiceViewModel model);

        ReportPrepaidMoneyCardViewModel GetReportPrepaidMoneyCard(ReportFilterModel filter);
        byte[] GetExcelPrepaidMoneyCard(ReportPrepaidMoneyCardViewModel model);

        ReportPrepaidServiceCardViewModel GetReportPrepaidServiceCard(ReportFilterModel filter);
        byte[] GetExcelPrepaidServiceCard(ReportPrepaidServiceCardViewModel model);

        ReportDebtViewModel GetReportDebt(ReportFilterModel filter);
        byte[] GetExcelDebt(ReportDebtViewModel model);
    }
}
