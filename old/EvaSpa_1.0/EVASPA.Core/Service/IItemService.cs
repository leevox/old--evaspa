﻿using System.Linq;
using EVASPA.Model.DataModel;

namespace EVASPA.Core.Service
{
    public interface IItemService : IService<Item>
    {
        IQueryable<Item> GetAllItemsWhoseQuantityIsEqualOrLowerThan(int limit);

        IQueryable<Product> GetAllProducts();
        Product GetProductById(int? id);

        IQueryable<Model.DataModel.Service> GetAllServices();
        Model.DataModel.Service GetServiceById(int? id);

        IQueryable<CardType> GetAllCardTypes();
        CardType GetCardTypeById(int? id);

        void Import(ItemHistory model);
    }
}