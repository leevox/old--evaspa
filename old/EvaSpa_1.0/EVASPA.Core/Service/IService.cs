﻿using System.Linq;
using EVASPA.Model.DataModel;

namespace EVASPA.Core.Service
{
    public interface IService<TEntity> where TEntity : EntityBase
    {
        IQueryable<TEntity> AllEntities { get; }

        IQueryable<TEntity> AllEntitiesIncludedDeleted { get; }

        TEntity GetById(int? entityId, bool includeDeleted = false);

        TEntity Add(TEntity entity);
        TEntity Update(TEntity entity);
        void Delete(TEntity entity);
        void Delete(int entityId);
    }
}
