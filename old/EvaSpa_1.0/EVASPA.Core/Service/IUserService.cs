﻿using System.Collections.Generic;
using EVASPA.Model.DataModel;

namespace EVASPA.Core.Service
{
    public interface IUserService : IService<User>
    {
        User ValidateUser(string userName, string password);
        IEnumerable<User> AllEmployees { get; }
        void TakeLeave(Leaving model);
    }
}
