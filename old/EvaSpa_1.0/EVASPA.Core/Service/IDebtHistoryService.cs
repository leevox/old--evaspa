﻿using EVASPA.Model.DataModel;

namespace EVASPA.Core.Service
{
    public interface IDebtHistoryService : IService<DebtHistory>
    {

    }
}