﻿using EVASPA.Model.DataModel;
using System.Linq;

namespace EVASPA.Core.Service
{
    public interface IPrepaidCardService : IService<PrepaidCard>
    {
        IQueryable<PrepaidMoneyCard> AllMoneyCards { get; }

        IQueryable<PrepaidCard> AllCardsWithPreLoadedInfo { get; }

        decimal GetCardAvailable(PrepaidCard card);

        string GenerateNewCardSerialNo();
        PrepaidCard GetBySerialNumber(string serialNumber);

        void UseServiceCard(PrepaidServiceCardHistory history, int[] performers);

        void LockCard(string serialNumber);

        int DeleteHistory(int id);
    }
}