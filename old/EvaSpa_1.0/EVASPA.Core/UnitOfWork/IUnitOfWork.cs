﻿using System.Data.Entity;
using EVASPA.Model.DataModel;

namespace EVASPA.Core.UnitOfWork
{
    public enum EvaEntityState
    {
        Added,
        Updated,
        Deleted,
        Unchanged,
        Detached
    }

    public interface IUnitOfWork
    {
        TEntity GetEntityInTracker<TEntity>(int id) where TEntity : EntityBase;
        DbSet<TEntity> GetEntities<TEntity>() where TEntity : EntityBase ;
        void SetState<TEntity>(TEntity entity, EvaEntityState state) where TEntity : EntityBase;
        void SaveChanges();
    }
}
