﻿using System.Data.Entity;
using EVASPA.Model.DataModel;

namespace EVASPA.Core.UnitOfWork
{
    public interface IEvaContext
    {
       DbSet<Bill> Bills { get; set; }

       DbSet<BillItem> BillItems { get; set; }

       DbSet<Customer> Customers { get; set; }

       DbSet<Debt> Debts { get; set; }

       DbSet<DebtHistory> DebtHistories { get; set; }

       DbSet<Item> Items { get; set; }

       DbSet<ItemHistory> ItemHistories { get; set; }

       DbSet<PrepaidCard> PrepaidCards { get; set; }

       DbSet<Leaving> Leavings { get; set; }

       DbSet<User> Users { get; set; }

       DbSet<PrepaidCardHistory> PrepaidCardHistories { get; set; }
    }
}
