﻿using OfficeOpenXml;
using System;
using System.Drawing;
using System.Linq;

namespace EVASPA.Core.Utils
{
    public class ExcelHelper
    {
        ExcelPackage _excel = new ExcelPackage();
        ExcelWorksheet _activeSheet;

        public ExcelHelper(string initSheet)
        {
            _activeSheet = AddSheet(initSheet);
        }

        public byte[] GetContent()
        {
            return _excel.GetAsByteArray();
        }

        #region sheet

        public ExcelWorksheet AddSheet(string sheetName)
        {
            return _excel.Workbook.Worksheets.Add(sheetName);
        }

        public ExcelWorksheet GetSheet(string sheetName)
        {
            return _excel.Workbook.Worksheets.FirstOrDefault(x => string.Compare(x.Name, sheetName, true) == 0);
        }

        public ExcelWorksheet SetActiveSheet(string sheetName)
        {
            _activeSheet = GetSheet(sheetName);
            return _activeSheet;
        }

        #endregion

        #region set value

        public void SetValue(string cellAddress, object value)
        {
            SetValue(_activeSheet, cellAddress, value);
        }

        public void SetValue(string sheetName, string cellAddress, object value)
        {
            SetValue(GetSheet(sheetName), cellAddress, value);
        }

        public void SetValue(ExcelWorksheet sheet, string cellAddress, object value)
        {
            var c = sheet.Cells[cellAddress];
            c.Value = value;

            SetDisplayFormatAuto(c, value);
        }

        #endregion

        #region set values

        public void SetValuesInRow(string cellAddress, params object[] values)
        {
            SetValuesInRow(_activeSheet, cellAddress, values);
        }

        public void SetValuesInRow(string sheetName, string cellAddress, params object[] values)
        {
            SetValuesInRow(GetSheet(sheetName), cellAddress, values);
        }

        public void SetValuesInRow(ExcelWorksheet sheet, string cellAddress, params object[] values)
        {
            if (values == null && values.Length <= 0)
                return;

            var cell = sheet.Cells[cellAddress];
            var row = cell.Start.Row;
            var col = cell.Start.Column;

            for (int i = 0; i < values.Length; i++)
            {
                SetValue(sheet, GetCellName(row, col + i), values[i]);
            }
        }

        #endregion

        #region set formula

        public void SetFormula(string cellAddress, string formula)
        {
            SetFormula(_activeSheet, cellAddress, formula);
        }

        public void SetFormula(string sheetName, string cellAddress, string formula)
        {
            SetFormula(GetSheet(sheetName), cellAddress, formula);
        }

        public void SetFormula(ExcelWorksheet sheet, string cellAddress, string formula)
        {
            var cell = sheet.Cells[cellAddress];
            cell.Formula = formula;
        }

        #endregion

        #region display format

        public void SetDisplayFormatAuto(string cellAddress, object value)
        {
            SetDisplayFormatAuto(_activeSheet, cellAddress, value);
        }

        public void SetDisplayFormatAuto(ExcelWorksheet sheet, string cellAddress, object value)
        {
            SetDisplayFormatAuto(sheet.Cells[cellAddress], value);
        }

        public void SetDisplayFormatAuto(ExcelRange cell, object value)
        {
            if (value is DateTime)
            {
                SetDisplayFormat(cell, "dd/MM/yyyy");
            }
            else if (value is int || value is long)
            {
                SetDisplayFormat(cell, "#,#");
            }
            else if (value is float || value is double)
            {
                SetDisplayFormat(cell, "#,##0.0#;#,##0.0#;#");
            }
        }

        public void SetDisplayFormat(string cellAddress, string format)
        {
            SetDisplayFormat(_activeSheet, cellAddress, format);
        }

        public void SetDisplayFormat(ExcelWorksheet sheet, string cellAddress, string format)
        {
            SetDisplayFormat(sheet.Cells[cellAddress], format);
        }

        public void SetDisplayFormat(ExcelRange cell, string format)
        {
            cell.Style.Numberformat.Format = format;
        }

        #endregion

        #region display style

        public void SetDisplayStyle(string cellAddress, CellStyle style)
        {
            SetDisplayStyle(_activeSheet, cellAddress, style);
        }

        public void SetDisplayStyle(ExcelWorksheet sheet, string cellAddress, CellStyle style)
        {
            SetDisplayStyle(sheet.Cells[cellAddress], style);
        }

        public void SetDisplayStyle(ExcelRange cell, CellStyle style)
        {
            cell.Style.Font.Bold = style.FontStyle == FontStyle.Bold;
            cell.Style.Font.Italic = style.FontStyle == FontStyle.Italic;
            cell.Style.Font.UnderLine = style.FontStyle == FontStyle.UnderLine;

            if (style.FontColor != null)
                cell.Style.Font.Color.SetColor(style.FontColor);
            if (style.FontSize > 0)
                cell.Style.Font.Size = style.FontSize;
            if (style.BackgroundColor != Color.Transparent)
            {
                cell.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                cell.Style.Fill.BackgroundColor.SetColor(style.BackgroundColor);
            }
        }

        #endregion

        #region others

        public static string GetCellName(int row, int col)
        {
            return ExcelRange.GetAddress(row, col);
        }

        [Flags]
        public enum FontStyle { Bold,Italic,UnderLine }

        public class CellStyle
        {
            public FontStyle FontStyle { get; set; }
            public float FontSize { get; set; }
            public Color FontColor { get; set; }
            public Color BackgroundColor { get; set; }

            public CellStyle()
            {
                BackgroundColor = Color.Transparent;
            }
        }

        #endregion
    }
}
