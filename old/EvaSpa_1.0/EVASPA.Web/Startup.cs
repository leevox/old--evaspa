﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EVASPA.Web.Startup))]
namespace EVASPA.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
