﻿When update:
	+ Update bootstrap-select:
		+ Edit bootstrap-select.js:

			---------- (line 13) -------------------------------------------------
			$.expr[':'].icontains = function (obj, index, meta) {
				return _search(meta[3], $(obj).attr('trimtext'));
			};



			------ createLi: function() {...} (line 181) -------------------------
			$.each(_liA, function(i, item) {
                _liHtml += '<li rel=' + i + '>' + item + '</li>';
            });

            _liHtml = _addSearchData($(_liHtml));



			---------- render: function() {...} (line 249) -----------------------
			var tooltip = _createToolTip(title);

            this.$button.attr('title', $.trim(tooltip));
            this.$newElement.find('.filter-option').html(title);