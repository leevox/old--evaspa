﻿function _removeVietnamese(pattern) {
    var copy = pattern;
    var vi = ['aáàảãạăắằẳẵặâấầẩẫậÁÀẢÃẠĂẮẰẲẴẶÂẤẦẨẪẬ', 'dđĐ', 'eéèẻẽẹêếềểễệÉÈẺẼẸÊẾỀỂỄỆ', 'iíìỉĩịÍÌỈĨỊ', 'oóòỏõọơớờởỡợôốồổỗộÓÒỎÕỌƠỚỜỞỠỢÔỐỒỔỖỘ', 'uúùủũụưứừửữựÚÙỦŨỤƯỨỪỬỮỰ', 'yýỳỷỹỵÝỲỶỸỴ'];
    for (var i = 0; i < vi.length; i++) {
        for (var c = 0; c < vi[i].length; c++) {
            var firstChar = vi[i].charAt(0);
            copy = copy.replace(vi[i].charAt(c), firstChar);
        }
    }

    return copy;
};

function _createToolTip(text) {
    var ret = '';
    var objs = $.parseHTML(text);
    for (var i = 0; i < objs.length; i++) {
        ret += $(objs[i]).text();
    }
    var r = ret.replace(/\[.+?\]/g, '');
    return r;
}

function _addSearchData(list) {
    var ret = '';
    for (var i = 0; i < list.length; i++) {
        ret += $(list[i]).attr('trimtext', _removeVietnamese($(list[i]).text()).toUpperCase()).wrap('<div>').parent().html();
    }
    return ret;
};

function _search(pattern, trimtext) {
    var trimpattern = _removeVietnamese(pattern).toUpperCase();
    if (trimtext == undefined) {
        return -1;
    }
    return trimtext.indexOf(trimpattern) >= 0;
}