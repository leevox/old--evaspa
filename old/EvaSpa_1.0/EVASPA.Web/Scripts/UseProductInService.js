﻿function UseNewProduct(name) {
    var divUseProducts = $('div#divUseProducts');
    var newproduct = divUseProducts.find('div.useproduct.hidden').clone().removeClass('hidden');

    var cbo = $('#AllProducts').clone().removeClass('hidden').addClass('product');
    if (name != null) {
        cbo.attr("name", name);
    }

    cbo.appendTo(newproduct.find('div.useproduct-allproducts'));
    cbo.selectpicker();

    newproduct.find('input.quantity').addClass('product');
    newproduct.appendTo(divUseProducts);
}