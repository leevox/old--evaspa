﻿function formatNumber(x) {
    if ('number' == typeof x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    } else if (!isNaN(x) && 'string' == typeof x) {
        return parseFloat(x).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    } else {
        return x.toString();
    }
};

function removeFormat(x) {
    return x.toString().replace(/\,/g, '');
};

function UpdateMoneyBack() {
    var billMoney = parseFloat(removeFormat($('#lblBillMoney').text()));
    var customerMoney = parseFloat($('#CustomerMoney').val());
    var moneycards = 0;
    $('#divAllMoneyCards a.badge span.important').each(function() {
        moneycards += parseFloat(removeFormat($(this).text()));
    });
    var moneyBack = customerMoney + moneycards - billMoney;
    var lblMoneyback = $('#lblMoneyBack');
    lblMoneyback.text(formatNumber(moneyBack));
    if (moneyBack < 0) {
        lblMoneyback.removeClass('green');
    } else {
        lblMoneyback.addClass('green');
    }
};

function UpdatBillMoney() {
    var billMoney = 0;
    $('#cart tr:not(.hidden) td.money').each(function () {
        billMoney += parseFloat(removeFormat($(this).text()));
    });

    $('#lblBillMoney').text(formatNumber(billMoney));
    UpdateMoneyBack();
};

function AddMoneyCard() {
    var cardSerialNo = $.trim($('#UseMoneyCard_txtPrice').val());

    $.ajax({
        url: UseMoneyCardUrl,
        data: { id: cardSerialNo },
        type: "POST"
    })
    .done(function (data) {
        if (data.success == true) {
            var existed = false;
            $('#divAllMoneyCards a.badge input[type=hidden][value=' + data.cardId + ']').each(function() {
                existed = true;
            });

            if (existed) {
                $('#UseMoneyCard_lblError').text('Đã nhập thẻ này vào giỏ hàng rồi.');
                return;
            }

            $('<div style="padding-bottom: 5px;text-transform: uppercase;"><a href="#" onclick="$(this).parent().remove(); UpdateMoneyBack(); return false;" class="badge active"><input name="UsedMoneyCards[0]" type="hidden" value="' + data.cardId + '"><span class="cardNo">[' + cardSerialNo + ']</span> <span class="important red">' + formatNumber(data.available) + '</span></a></div>')
           .appendTo($('#divAllMoneyCards'));

            $('#UseMoneyCard_txtPrice').val('');
            $('#UseMoneyCard_lblError').text('');
            UpdateMoneyBack();
            $('#modalUseMoneyCard').modal('hide');
        } else {
            $('#UseMoneyCard_lblError').text(data.msg);
        }
    })
    .fail(function () {
        $('#UseMoneyCard_lblError').text('Không thể xử lý thông tin. Vui lòng thử lại sau.');
    });
};

function AddProductToCart(id, name, quantity, price, discount, money)
{
    var x = $('#itemTemplate').clone().removeAttr('id').removeClass('hidden').addClass('product');

    x.find('td.quantity').text(formatNumber(quantity));
    x.find('td.price').text(formatNumber(price));
    x.find('td.discount').text(formatNumber(discount));
    x.find('td.money').text(money);

    x.find('td.text').html('Sản phẩm - <strong>' + name + '</strong>');

    var inputs = x.find('td.hidden');
    $('<input type="hidden" name="Products[0].ProductId" value="' + id + '">').appendTo(inputs);
    $('<input type="hidden" name="Products[0].Quantity" value="' + quantity + '">').appendTo(inputs);
    $('<input type="hidden" name="Products[0].Price" value="' + price + '">').appendTo(inputs);
    $('<input type="hidden" name="Products[0].Discount" value="' + discount + '">').appendTo(inputs);

    Cart.append(x);
    UpdatBillMoney();
};

function AddServiceToCart(id, name, quantity, price, discount, money, performerIds, performerNames, products) {
    var x = $('#itemTemplate').clone().removeAttr('id').removeClass('hidden').addClass('service');

    x.find('td.quantity').text(formatNumber(quantity));
    x.find('td.price').text(formatNumber(price));
    x.find('td.discount').text(formatNumber(discount));
    x.find('td.money').text(money);

    var inputs = x.find('td.hidden');
    $('<input type="hidden" name="Services[0].ServiceId" value="' + id + '">').appendTo(inputs);
    $('<input type="hidden" name="Services[0].Quantity" value="' + quantity + '">').appendTo(inputs);
    $('<input type="hidden" name="Services[0].Price" value="' + price + '">').appendTo(inputs);
    $('<input type="hidden" name="Services[0].Discount" value="' + discount + '">').appendTo(inputs);

    for (var i = 0; i < performerIds.length; i++) {
        $('<input type="hidden" name="Services[0].PerformerIds[' + i + ']" value="' + performerIds[i] + '">').appendTo(inputs);
    }
    
    var usedProductsText = '';
    for (var i = 0; i < products.length; i++) {
        $('<input type="hidden" name="Services[0].UsedProductIds[' + i + ']" value="' + products[i].id + '">').appendTo(inputs);
        $('<input type="hidden" name="Services[0].UsedProductCounts[' + i + ']" value="' + products[i].quantity + '">').appendTo(inputs);
        usedProductsText += products[i].name + ' (' + products[i].quantity + '), ';
    }
    if (usedProductsText.length != 0) {
        usedProductsText = '<div class="sub">Dùng kèm: <em>' + usedProductsText.replace(/\), $/, ')') + '</em></div>';
    }

    var text = '<div>Dịch vụ - <strong>' + name + '</strong></div><div class="sub">Nhân viên: <em>' + performerNames.join(", ") + '</em></div>' + usedProductsText;
    x.find('td.text').html(text);

    Cart.append(x);
    UpdatBillMoney();
};

function AddMoneyCardToCart(id, name, quantity, price, discount, money) {
    var x = $('#itemTemplate').clone().removeAttr('id').removeClass('hidden').addClass('moneycard');

    x.find('td.quantity').text(formatNumber(quantity));
    x.find('td.price').text(formatNumber(price));
    x.find('td.discount').text(formatNumber(discount));
    x.find('td.money').text(money);

    x.find('td.text').html('Phiếu quà tặng - <strong>' + name + '</strong><div class="sub">Tiền hạn mức: <em>' + formatNumber(price) + '</em></div>');

    var inputs = x.find('td.hidden');
    $('<input type="hidden" name="PrepaidMoneyCards[0].CardTypeId" value="' + id + '">').appendTo(inputs);
    $('<input type="hidden" name="PrepaidMoneyCards[0].Quota" value="' + price + '">').appendTo(inputs);
    $('<input type="hidden" name="PrepaidMoneyCards[0].Discount" value="' + discount + '">').appendTo(inputs);

    Cart.append(x);
    UpdatBillMoney();
};

function AddServiceCardToCart(id, name, quantity, price, discount, money, serviceId, serviceName) {
    var x = $('#itemTemplate').clone().removeAttr('id').removeClass('hidden').addClass('servicecard');

    x.find('td.quantity').text(formatNumber(quantity));
    x.find('td.price').text(formatNumber(price));
    x.find('td.discount').text(formatNumber(discount));
    x.find('td.money').text(money);

    x.find('td.text').html('Phiếu dịch vụ - <strong>' + name + '</strong><div class="sub">Dịch vụ: <em>' + serviceName + '</em></div><div class="sub">Số suất: <em>' + quantity + '</em></div>');

    var inputs = x.find('td.hidden');
    $('<input type="hidden" name="PrepaidServiceCards[0].CardTypeId" value="' + id + '">').appendTo(inputs);
    $('<input type="hidden" name="PrepaidServiceCards[0].Quota" value="' + quantity + '">').appendTo(inputs);
    $('<input type="hidden" name="PrepaidServiceCards[0].Price" value="' + price + '">').appendTo(inputs);
    $('<input type="hidden" name="PrepaidServiceCards[0].Discount" value="' + discount + '">').appendTo(inputs);
    $('<input type="hidden" name="PrepaidServiceCards[0].ServiceId" value="' + serviceId + '">').appendTo(inputs);

    Cart.append(x);
    UpdatBillMoney();
};

function ClearAddForm(txtQuantity, txtDiscount, lblMoney, cbo1) {
    if (txtQuantity != undefined) {
        txtQuantity.val(0);
    }
    txtDiscount.val(0);
    lblMoney.text('0');
    if (cbo1 != undefined) {
        cbo1.selectpicker('deselectAll');
    }
};

function RefreshAddForm(event) {
    var price = event.data.txtPrice.val();
    var quantity = event.data.txtQuantity.val();
    var discount = event.data.txtDiscount.val();

    var money = price * quantity - discount;
    event.data.lblMoney.text(formatNumber(money));
};

function UpdatePriceThenRefresh(event) {
    var selectedItemId = event.data.cboItem.val();
    var selectedItem = event.data.cboItem.children('[value=' + selectedItemId + ']');
    var price = selectedItem.attr('price');
    if (event.data.txtQuantityInStock != null && event.data.txtQuantityInStock != undefined) {
        event.data.txtQuantityInStock.val(selectedItem.attr('quantityinstock'));
    }
    event.data.txtPrice.val(price);
    RefreshAddForm(event);
};

function ConfirmSubmit(form) {
    var index = 0;
    $('#cart tr.product:not(.hidden) td.hidden').each(function () {
        $(this).find('input[type=hidden]').each(function () {
            $(this).attr('name', $(this).attr('name').replace(/\[\d*\]/, '[' + index + ']'));
        });
        index++;
    });

    index = 0;
    $('#cart tr.service:not(.hidden) td.hidden').each(function () {
        $(this).find('input[type=hidden]').each(function () {
            $(this).attr('name', $(this).attr('name').replace(/\[\d*\]/, '[' + index + ']'));
        });
        index++;
    });

    index = 0;
    $('#cart tr.moneycard:not(.hidden) td.hidden').each(function () {
        $(this).find('input[type=hidden]').each(function () {
            $(this).attr('name', $(this).attr('name').replace(/\[\d*\]/, '[' + index + ']'));
        });
        index++;
    });

    index = 0;
    $('#cart tr.servicecard:not(.hidden) td.hidden').each(function () {
        $(this).find('input[type=hidden]').each(function () {
            $(this).attr('name', $(this).attr('name').replace(/\[\d*\]/, '[' + index + ']'));
        });
        index++;
    });

    index = 0;
    $('#divAllMoneyCards input[type=hidden]').each(function () {
        $(this).attr('name', $(this).attr('name').replace(/\[\d*\]/, '[' + index + ']'));
        index++;
    });

    $('input#SellerId').val(cboSellerId.val());
    $('input#CustomerId').val(cboCustomerId.val());

    var data = form.serializeArray();

    $.ajax({
        url: form.attr('action'),
        data: data,
        type: "POST"
    })
    .done(function (ret) {
        if (ret.success) {
            window.location.replace(ret.msg);
        } else {
            alert("Lỗi: " + ret.msg);
        }
    })
    .fail(function () {
        alert('Không thể xử lý thông tin. Vui lòng thử lại sau.');
    });
};

function ValidateForm() {
    if ($('#cart tr:not(.hidden)').length <= 0) {
        alert("Giỏ hàng không thể trống.");
        return;
    }

    if ($('#CustomerMoney').val() < 0) {
        alert("Tiền khách hàng không thể âm.");
        return;
    }

    var moneyBack = parseFloat(removeFormat($('#lblMoneyBack').text()));
    if (moneyBack >= 0) {
        $('#lblCreateDebt').addClass('hidden');
    } else {
        $('#lblCreateDebt').removeClass('hidden');
    }
    $('#modalConfirmSubmit').modal();
};

$(document).ready(function() {
    $('.datepicker').datepicker({ format: 'dd/mm/yyyy' });


    Cart = $('#cart');

    $('#CustomerMoney').change(function () {
         UpdateMoneyBack();
    });

    cboSellerId = $('#AllEmployees').clone().removeClass('hidden').attr('id', 'cboSellerId');
    cboSellerId.appendTo($('#divSellerId'));
    cboSellerId.selectpicker();

    cboCustomerId = $('#AllCustomers').clone().removeClass('hidden').attr('id', 'cboCustomerId');
    cboCustomerId.appendTo($('#divCustomerId'));
    cboCustomerId.selectpicker();

    AddProduct_cboAllProducts = $('#AllProducts').clone().removeClass('hidden').attr('id', 'AddProduct_cboAllProducts');
    AddProduct_cboAllProducts.appendTo($('#divAddProduct_cboAllProducts'));
    AddProduct_cboAllProducts.selectpicker();

    AddService_cboAllServices = $('#AllServices').clone().removeClass('hidden').attr('id', 'AddService_cboAllServices');
    AddService_cboAllServices.appendTo($('#divAddService_cboAllServices'));
    AddService_cboAllServices.selectpicker();

    AddService_cboAllEmployees = $('#AllEmployees').clone().removeClass('hidden')
        .attr('id', 'AddService_cboAllEmployees')
        .attr('multiple', 'true').attr('title', 'Chọn nhân viên');
    AddService_cboAllEmployees.appendTo($('#divAddService_cboAllEmployees'));
    AddService_cboAllEmployees.selectpicker();


    AddMoneyCard_cboAllCardTypes = $('#AllCardTypes').clone().removeClass('hidden').attr('id', 'AddMoneyCard_cboAllCardTypes');
    AddMoneyCard_cboAllCardTypes.appendTo($('#divAddMoneyCard_cboAllCardTypes'));
    AddMoneyCard_cboAllCardTypes.selectpicker();

    AddServiceCard_cboAllCardTypes = $('#AllCardTypes').clone().removeClass('hidden').attr('id', 'AddServiceCard_cboAllCardTypes');
    AddServiceCard_cboAllCardTypes.appendTo($('#divAddServiceCard_cboAllCardTypes'));
    AddServiceCard_cboAllCardTypes.selectpicker();

    AddServiceCard_cboAllServices = $('#AllServices').clone().removeClass('hidden').attr('id', 'AddServiceCard_cboAllServices');
    AddServiceCard_cboAllServices.appendTo($('#divAddServiceCard_cboAllServices'));
    AddServiceCard_cboAllServices.selectpicker();

    $('#UseMoneyCard_Add').click(function () {
        AddMoneyCard();
    });

    $('#frmCreateBill').submit(function (event) {
        event.preventDefault();
        ValidateForm();
    });

    $('#btnConfirmSubmitForm').click(function() {
        ConfirmSubmit($('#frmCreateBill'));
    });
});