﻿using EVASPA.Core.Service;
using System.Web.Mvc;

namespace EVASPA.Web.Controllers
{
    public abstract class ItemController : ControllerBase
    {
        protected IItemService ItemService { get; set; }

        protected ItemController(IItemService itemService)
        {
            ItemService = itemService;
        }

        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Create()
        {
            return View();
        }
	}
}