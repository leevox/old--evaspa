﻿using EVASPA.Core.Service;
using System;
using System.Web.Mvc;

namespace EVASPA.Web.Controllers
{
    public class HomeController : ControllerBase
    {
        private const int LowStockWarning = 5;

        private ICustomerService CustomerService { get; set; }
        private IItemService ItemService { get; set; }

        public HomeController(ICustomerService customerService, IItemService itemService)
        {
            CustomerService = customerService;
            ItemService = itemService;
        }

        public ActionResult Index()
        {
            var today = DateTime.Today;

            ViewBag.AllCustomerWhoHasBirthdayIsToday = CustomerService.GetAllCustomerWhoHasBirthdayIsInDay(today);
            ViewBag.AllCustomerWhoHasBirthdayIsInNextWeek = CustomerService.GetAllCustomerWhoHasBirthdayIsInRange(today, today.AddDays(7));
            ViewBag.LowStockItems = ItemService.GetAllItemsWhoseQuantityIsEqualOrLowerThan(LowStockWarning);

            return View();
        }
    }
}