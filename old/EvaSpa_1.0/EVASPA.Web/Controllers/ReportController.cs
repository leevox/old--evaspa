﻿using System;
using System.Globalization;
using EVASPA.Core.Service;
using EVASPA.Model.ViewModel;
using System.Web.Mvc;

namespace EVASPA.Web.Controllers
{
    [Authorize(Roles = "Admin,Manager")]
    public class ReportController : ControllerBase
    {
        public IReportService ReportService { get; set; }

        public ReportController(IReportService reportService)
        {
            ReportService = reportService;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(ReportFilterModel model, string startDate, string endDate)
        {
            DateTime start, end;
            if (!DateTime.TryParseExact(startDate, "dd/MM/yyyy", null, DateTimeStyles.None, out start))
                start = DateTime.Now.Date;
            if (!DateTime.TryParseExact(endDate, "dd/MM/yyyy", null, DateTimeStyles.None, out end))
                end = DateTime.Now.Date;

            model.StartDate = start;
            model.EndDate = end;

            if (model.StartDate > model.EndDate)
            {
                ModelState.Clear();
                ModelState.AddModelError("","Ngày báo cáo không phù hợp.");
                return View(model);
            }

            switch (model.Type)
            {
                case ReportType.Employee:
                    return RedirectToAction("Employee", model);
                case ReportType.PrepaidMoneyCard:
                    return RedirectToAction("PrepaidMoneyCard", model);
                case ReportType.PrepaidServiceCard:
                    return RedirectToAction("PrepaidServiceCard", model);
                case ReportType.Product:
                    return RedirectToAction("Product", model);
                case ReportType.Service:
                    return RedirectToAction("Service", model);
                case ReportType.Debt:
                    return RedirectToAction("Debt", model);
            }
            return HttpNotFound();
        }

        public ActionResult Employee(ReportFilterModel model)
        {
            if (model.ExportToExcel == true)
            {
                return Excel(model);
            }
            else
            {
                var ret = ReportService.GetReportEmployee(model);
                return View(ret);
            }
        }

        public ActionResult Product(ReportFilterModel model)
        {
            if (model.ExportToExcel == true)
            {
                return Excel(model);
            }
            else
            {
                var ret = ReportService.GetReportProduct(model);
                return View(ret);
            }
        }

        public ActionResult Service(ReportFilterModel model)
        {
            if (model.ExportToExcel == true)
            {
                return Excel(model);
            }
            else
            {
                var ret = ReportService.GetReportService(model);
                return View(ret);
            }
        }

        public ActionResult PrepaidMoneyCard(ReportFilterModel model)
        {
            if (model.ExportToExcel == true)
            {
                return Excel(model);
            }
            else
            {
                var ret = ReportService.GetReportPrepaidMoneyCard(model);
                return View(ret);
            }
        }

        public ActionResult PrepaidServiceCard(ReportFilterModel model)
        {
            if (model.ExportToExcel == true)
            {
                return Excel(model);
            }
            else
            {
                var ret = ReportService.GetReportPrepaidServiceCard(model);
                return View(ret);
            }
        }

        public ActionResult Debt(ReportFilterModel model)
        {
            if (model.ExportToExcel == true)
            {
                return Excel(model);
            }
            else
            {
                var ret = ReportService.GetReportDebt(model);
                return View(ret);
            }
        }

        public ActionResult Excel(ReportFilterModel model)
        {
            byte[] content = null;
            string type = string.Empty;

            switch (model.Type)
            {
                case ReportType.Debt:
                    content = ReportService.GetExcelDebt(ReportService.GetReportDebt(model));
                    type = "Bao Cao No";
                    break;
                case ReportType.Employee:
                    content = ReportService.GetExcelEmployee(ReportService.GetReportEmployee(model));
                    type = "Bao Cao Nhan Vien";
                    break;
                case ReportType.PrepaidMoneyCard:
                    content = ReportService.GetExcelPrepaidMoneyCard(ReportService.GetReportPrepaidMoneyCard(model));
                    type = "Bao Cao Phieu Qua Tang";
                    break;
                case ReportType.PrepaidServiceCard:
                    content = ReportService.GetExcelPrepaidServiceCard(ReportService.GetReportPrepaidServiceCard(model));
                    type = "Bao Cao Phieu Dich Vu";
                    break;
                case ReportType.Product:
                    content = ReportService.GetExcelProduct(ReportService.GetReportProduct(model));
                    type = "Bao Cao San Pham";
                    break;
                case ReportType.Service:
                    content = ReportService.GetExcelService(ReportService.GetReportService(model));
                    type = "Bao Cao Dich vu";
                    break;
            }

            var fileName = string.Format("{0}_{1:ddMMyyyy}_{2:ddMMyyyy}.xlsx", type, model.StartDate, model.EndDate);
            return File(content, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
        }
	}
}