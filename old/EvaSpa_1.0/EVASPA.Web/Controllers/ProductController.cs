﻿using EVASPA.Core.Service;
using System.Net;
using System.Web.Mvc;
using EVASPA.Model.DataModel;
using System.Linq;

namespace EVASPA.Web.Controllers
{
    public class ProductController : ItemController
    {
        protected IItemHistoryService ItemHistoryService { get; set; }

        public ProductController(IItemService itemService, IItemHistoryService itemHistoryService)
            : base(itemService)
        {
            ItemHistoryService = itemHistoryService;
        }

        public ActionResult Index()
        {
            var allItems = ItemService.GetAllProducts();
            return View(allItems);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var item = ItemService.GetProductById(id);
            if (item == null)
            {
                return HttpNotFound();
            }

            ViewBag.ImportHistories = ItemHistoryService.AllEntities.Where(x => x.ItemId == id).OrderByDescending(x => x.Date);
            return View(item);
        }

        [Authorize(Roles = "Admin,Manager")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,DefaultPrice,QuantityInStock,Discontinued")] Product product)
        {
            if (ModelState.IsValid)
            {
                ItemService.Add(product);
                return RedirectToAction("Index");
            }
            return View(product);
        }

        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var item = ItemService.GetProductById(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }

        [Authorize(Roles = "Admin,Manager")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,DefaultPrice,QuantityInStock,Discontinued")] Product product)
        {
            if (ModelState.IsValid)
            {
                ItemService.Update(product);
                return RedirectToAction("Index");
            }
            return View(product);
        }

        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var item = ItemService.GetProductById(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }
    }
}
