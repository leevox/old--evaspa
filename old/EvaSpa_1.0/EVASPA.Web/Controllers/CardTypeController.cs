﻿using EVASPA.Core.Service;
using EVASPA.Model.DataModel;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace EVASPA.Web.Controllers
{
    public class CardTypeController : ItemController
    {
        protected IItemHistoryService ItemHistoryService { get; set; }

        public CardTypeController(IItemService itemService, IItemHistoryService itemHistoryService)
            : base(itemService)
        {
            ItemHistoryService = itemHistoryService;
        }

        public ActionResult Index()
        {
            var allItems = ItemService.GetAllCardTypes();
            return View(allItems);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var item = ItemService.GetCardTypeById(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            ViewBag.ImportHistories = ItemHistoryService.AllEntities.Where(x => x.ItemId == id).OrderByDescending(x => x.Date);
            return View(item);
        }

        [Authorize(Roles = "Admin,Manager")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,DefaultPrice,QuantityInStock,Discontinued")] CardType cardType)
        {
            if (ModelState.IsValid)
            {
                ItemService.Add(cardType);
                return RedirectToAction("Index");
            }
            return View(cardType);
        }

        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var item = ItemService.GetCardTypeById(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }

        [Authorize(Roles = "Admin,Manager")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,DefaultPrice,QuantityInStock,Discontinued")] CardType cardType)
        {
            if (ModelState.IsValid)
            {
                ItemService.Update(cardType);
                return RedirectToAction("Index");
            }
            return View(cardType);
        }

        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var item = ItemService.GetCardTypeById(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }
    }
}
