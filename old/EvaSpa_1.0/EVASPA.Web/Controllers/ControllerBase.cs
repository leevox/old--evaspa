﻿using System;
using System.Web.Configuration;
using System.Web.Mvc;

namespace EVASPA.Web.Controllers
{
    [Authorize]
    [MaintainingActionFilter]
    [HandleError]
    public abstract class ControllerBase : Controller
    {
        public ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
    }

    public class MaintainingActionFilter : ActionFilterAttribute
    {
        public static int? IsMaintaining = null;

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (IsMaintaining == null)
            {
                IsMaintaining = Convert.ToInt32(WebConfigurationManager.AppSettings["IsMaintaining"]);
            }

            if (IsMaintaining.Value != 0)
                filterContext.Result = new RedirectResult("~/IsMaintaining.html");
        }
    }
}