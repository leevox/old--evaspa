﻿using EVASPA.Core.Service;
using System.Net;
using System.Web.Mvc;

namespace EVASPA.Web.Controllers
{
    public class ServiceController : ItemController
    {
        public ServiceController(IItemService itemService) : base(itemService)
        {

        }

        public ActionResult Index()
        {
            var allItems = ItemService.GetAllServices();
            return View(allItems);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var item = ItemService.GetServiceById(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }

        [Authorize(Roles = "Admin,Manager")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,DefaultPrice,Discontinued")] Model.DataModel.Service service)
        {
            if (ModelState.IsValid)
            {
                ItemService.Add(service);
                return RedirectToAction("Index");
            }
            return View(service);
        }

        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var item = ItemService.GetServiceById(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }

        [Authorize(Roles = "Admin,Manager")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,DefaultPrice,Discontinued")] Model.DataModel.Service service)
        {
            if (ModelState.IsValid)
            {
                ItemService.Update(service);
                return RedirectToAction("Index");
            }
            return View(service);
        }

        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var item = ItemService.GetServiceById(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }
    }
}
