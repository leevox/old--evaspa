﻿using System.Linq;
using System.Web.DynamicData;
using EVASPA.Core.Service;
using EVASPA.Model.DataModel;
using EVASPA.Model.ViewModel;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Net;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace EVASPA.Web.Controllers
{
    [Authorize(Roles = "Admin,Manager")]
    public class UserController : ControllerBase
    {
        private ILeavingService LeavingService { get; set; }
        private IUserService UserService { get; set; }

        private IAuthenticationManager AuthenticationManager
        {
            get { return HttpContext.GetOwinContext().Authentication; }
        }

        public UserController(IUserService userService, ILeavingService leavingService)
        {
            UserService = userService;
            LeavingService = leavingService;
        }

        private ClaimsIdentity CreateIdentity(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.DisplayName),
                new Claim(ClaimTypes.Role, user.UserType.ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString())
            };
            return new ClaimsIdentity(claims, "ApplicationCookie");
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
#if DEBUG
            var identity = CreateIdentity(UserService.ValidateUser("admin", "welkom01"));
            AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = true }, identity);
#else
            ViewBag.ReturnUrl = returnUrl;
#endif
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var user = UserService.ValidateUser(model.UserName, model.Password);
                if (user != null)
                {
                    var identity = CreateIdentity(user);
                    AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = model.RememberMe }, identity);
                    return RedirectToLocal(returnUrl);
                }
                else
                {
                    ModelState.AddModelError("", "Mật khẩu không đúng hoặc tài khoản không tồn tại.");
                }
            }

            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Index()
        {
            var model = UserService.AllEntities;
            if (!User.IsInRole("Admin"))
            {
                model = model.Where(x => x.UserType >= UserType.Manager);
            }
            return View(model);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = UserService.GetById(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            ViewBag.LeavingHistories = LeavingService.AllEntities.Where(x => x.UserId == id).OrderByDescending(x => x.Date);
            return View(user);
        }

        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Create([Bind(Include = "Id,UserName,DisplayName,Password,UserType")] User user)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (!User.IsInRole("Admin") && user.UserType == UserType.Admin)
                        throw new Exception("Không đủ quyền để thực hiện thao tác.");

                    UserService.Add(user);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View(user);
                }
                
            }

            return View(user);
        }

        // GET: /Customer/Edit/5
        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = UserService.GetById(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: /Customer/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,UserName,DisplayName,Password,UserType")] User user)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (!User.IsInRole("Admin") && user.UserType == UserType.Admin)
                        throw new Exception("Không đủ quyền để thực hiện thao tác.");

                    UserService.Update(user);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View(user);
                }
            }
            return View(user);
        }

        // GET: /Customer/Delete/5
        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = UserService.GetById(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: /Customer/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,Manager")]
        public ActionResult DeleteConfirmed(int id)
        {
            var user = UserService.GetById(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            try
            {
                if (string.Compare(user.Id.ToString(), User.Identity.GetUserId(), StringComparison.OrdinalIgnoreCase) == 0)
                    throw new Exception("Không thể xóa tài khoản đang đăng nhập.");
                if (!User.IsInRole("Admin") && user.UserType == UserType.Admin)
                    throw new Exception("Không đủ quyền để thực hiện thao tác.");
                UserService.Delete(id);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(user);
            }
            
        }

        [Authorize(Roles = "Admin,Manager")]
        public ActionResult TakeLeave(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = UserService.GetById(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            ViewBag.user = user;
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Manager")]
        public ActionResult TakeLeave(Leaving model, string sDate)
        {
            try
            {
                model.Date = DateTime.ParseExact(sDate, "dd/MM/yyyy", null);
                UserService.TakeLeave(model);
            }
            catch (Exception ex)
            {
                throw;
            }
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Admin,Manager")]
        public ActionResult DeleteLeaving(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var leaving = LeavingService.GetById(id);
            if (leaving == null)
            {
                return HttpNotFound();
            }
            return View(leaving);
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Manager")]
        public ActionResult DeleteLeaving(int id)
        {
            LeavingService.Delete(id);
            return RedirectToAction("Index");
        }
    }
}