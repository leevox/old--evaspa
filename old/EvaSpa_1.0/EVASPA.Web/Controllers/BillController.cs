﻿using System;
using System.Linq;
using EVASPA.Core.Service;
using System.Net;
using System.Web.Mvc;
using EVASPA.Model.DataModel;
using EVASPA.Model.ViewModel;

namespace EVASPA.Web.Controllers
{
    public class BillController : ControllerBase
    {
        private IBillService BillService { get; set; }
        private IItemService ItemService { get; set; }
        private IUserService UserService { get; set; }
        private ICustomerService CustomerService { get; set; }
        private IPrepaidCardService PrepaidCardService { get; set; }

        public BillController(IBillService billService, IItemService itemService, IUserService userService, ICustomerService customerService, IPrepaidCardService prepaidCardService)
        {
            BillService = billService;
            ItemService = itemService;
            UserService = userService;
            CustomerService = customerService;
            PrepaidCardService = prepaidCardService;
        }

        public ActionResult Index()
        {
            return View();
        }

        // GET: /Bill/
        //public ActionResult Index()
        //{
        //    return View(db.Bills.ToList());
        //}

        // GET: /Bill/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bill bill = BillService.GetById(id, true);
            if (bill == null)
            {
                return HttpNotFound();
            }
            return View(bill);
        }

        // GET: /Bill/Create
        public ActionResult Create()
        {
            ViewBag.BillId = BillService.NewBillId;
            ViewBag.AllProducts = ItemService.GetAllProducts();
            ViewBag.AllServices = ItemService.GetAllServices();
            ViewBag.AllCardTypes = ItemService.GetAllCardTypes();
            ViewBag.AllEmployees = UserService.AllEmployees;
            ViewBag.AllCustomers = CustomerService.AllEntities.OrderByDescending(x => x.IsVip);

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BillViewModel model)
        {
            var success = false;
            var msg = string.Empty;

            if (ModelState.IsValid)
            {
                try
                {
                    var bill = BillService.CreateBill(model);
                    msg = Url.Action("Details", "Bill", new {bill.Id});
                    success = true;
                }
                catch (Exception ex)
                {
                    success = false;
                    msg = ex.Message;
                }
            }
            return Json(new {success, msg});
        }

        // GET: /Bill/Delete/5
        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.BillId = id;
            return View();
        }

        // POST: /Bill/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,Manager")]
        public ActionResult DeleteConfirmed(int id)
        {
            BillService.Delete(id);
            return RedirectToAction("Details", new {id});
        }
    }
}
