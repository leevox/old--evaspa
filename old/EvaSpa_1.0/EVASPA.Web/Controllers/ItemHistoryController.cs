﻿using EVASPA.Core.Service;
using System.Net;
using System.Web.Mvc;
using EVASPA.Model.DataModel;
using System;
using System.Linq;

namespace EVASPA.Web.Controllers
{
    public class ItemHistoryController : ControllerBase
    {
        IItemService ItemService { get; set; }

        IItemHistoryService ItemHistoryService { get; set; }

        public ItemHistoryController(IItemService itemService, IItemHistoryService itemHistoryService)
        {
            ItemService = itemService;
            ItemHistoryService = itemHistoryService;
        }

        public ActionResult Import(int? itemId)
        {
            if (itemId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var item = ItemService.GetById(itemId.Value);
            if (item == null || !(item is Product || item is CardType))
            {
                return HttpNotFound();
            }
            ViewBag.Item = item;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Import([Bind(Include="Quantity,Price,Note")] ItemHistory itemhistory,string date, int itemId, string returnUrl)
        {
            var d = DateTime.ParseExact(date, "dd/MM/yyyy", null);
            itemhistory.Date = d;
            itemhistory.ItemId = itemId;

            if (ModelState.IsValid)
            {
                ItemService.Import(itemhistory);
            }

            return RedirectToLocal(returnUrl);
        }

        [Authorize(Roles = "Admin,Manager")]
        public ActionResult DeleteHistory(int? id)
        {
            var model = ItemHistoryService.AllEntities.FirstOrDefault(x => x.Id == id);
            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Manager")]
        public ActionResult DeleteHistory(int id)
        {
            ItemHistoryService.Delete(id);
            return RedirectToAction("Index", "Home");
        }
    }
}