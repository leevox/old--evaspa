﻿using System;
using System.Globalization;
using System.Linq;
using EVASPA.Core.Service;
using System.Net;
using System.Web.Mvc;
using EVASPA.Model.DataModel;

namespace EVASPA.Web.Controllers
{
    public class CustomerController : ControllerBase
    {
        private IReportService ReportService { get; set; }
        private ICustomerService CustomerService { get; set; }
        private IDebtService DebtService { get; set; }

        public CustomerController(ICustomerService customerService, IDebtService debtService, IReportService reportService)
        {
            CustomerService = customerService;
            DebtService = debtService;
            ReportService = reportService;
        }

        // GET: /Customer/
        public ActionResult Index()
        {
            var allCustomers = CustomerService.AllCustomersWithDebts().OrderByDescending(x=>x.IsVip).ToList();
            ViewBag.Service = CustomerService;
            return View(allCustomers);
        }

        // GET: /Customer/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var customer = CustomerService.GetById(id);
            if (customer == null)
            {
                return HttpNotFound();
            }

            var x = ReportService.GetReportDebt(new Model.ViewModel.ReportFilterModel
            {
                StartDate = DateTime.MinValue,
                EndDate = DateTime.MaxValue,
                CustomerIds = (new int[] { customer.Id }).ToList()
            });

            ViewBag.Service = CustomerService;
            ViewBag.DebtHistories = x.Lines;
            return View(customer);
        }

        // GET: /Customer/Create
        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Create(Customer customer, string dBirthday)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var bDay = DateTime.ParseExact(dBirthday, "dd/MM/yyyy", null);
                    customer.Birthday = bDay;
                }
                catch
                {
                    customer.Birthday = null;
                }

                CustomerService.Add(customer);
                return RedirectToAction("Index");
            }

            return View(customer);
        }

        // GET: /Customer/Edit/5
        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = CustomerService.GetById(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // POST: /Customer/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Edit(Customer customer, string dBirthday)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var bDay = DateTime.ParseExact(dBirthday, "dd/MM/yyyy", null);
                    customer.Birthday = bDay;
                }
                catch
                {
                    customer.Birthday = null;
                }

                CustomerService.Update(customer);
                return RedirectToAction("Index");
            }
            return View(customer);
        }

        // GET: /Customer/Delete/5
        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = CustomerService.GetById(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // POST: /Customer/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,Manager")]
        public ActionResult DeleteConfirmed(int id)
        {
            CustomerService.Delete(id);
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Admin,Manager")]
        public ActionResult PayDebt(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = CustomerService.GetById(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            ViewBag.Service = CustomerService;
            ViewBag.Customer = customer;
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Manager")]
        public ActionResult PayDebt(DebtHistory model, string sDate)
        {
            DateTime date = DateTime.Now;
            DateTime.TryParseExact(sDate, "dd/MM/yyyy", null, DateTimeStyles.None, out date);
            model.Date = date;

            DebtService.PayDebt(model);
            return RedirectToAction("Details", new {id = model.CustomerId});
        }

        [Authorize(Roles = "Admin,Manager")]
        public ActionResult DeleteDebtHistory(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var debtHistory = DebtService.GetDebtHistoryById(id);
            if (debtHistory == null)
            {
                return HttpNotFound();
            }
            return View(debtHistory);
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Manager")]
        public ActionResult DeleteDebtHistory(int id)
        {
            DebtService.DeleteDebtHistory(id);
            return RedirectToAction("Index");
        }
    }
}
