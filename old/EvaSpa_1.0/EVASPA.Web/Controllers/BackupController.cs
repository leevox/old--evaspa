﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Sdk.Sfc;
using Microsoft.SqlServer.Management.Smo;

namespace EVASPA.Web.Controllers
{
    [Authorize(Roles = "Admin,Manager")]
    public class BackupController : ControllerBase
    {
        private const string InitialCatalog = "Initial Catalog=";
        private const int BatchSize = 50;
        private static readonly string ConnectionString;
        private static readonly string Dbname;

        static BackupController()
        {
            string defaultConnection = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            int pos = defaultConnection.IndexOf(InitialCatalog, StringComparison.OrdinalIgnoreCase);

            if (pos < 0)
            {
                throw new Exception("DefaultConnection string is not correct");
            }

            string tmp = defaultConnection.Substring(pos);
            ConnectionString = defaultConnection.Remove(defaultConnection.Length - tmp.Length);
            Dbname = tmp.Substring(InitialCatalog.Length);
        }

        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "Admin,Manager")]
        public FileStreamResult Export(bool? IncludedLog)
        {
            string sqlScript = IncludedLog == true ? GenerateSqlScript(Dbname) : GenerateSqlScript(Dbname, "ELMAH_Error,__MigrationHistory");
            return File(GZipCompress(sqlScript), "application/gzip",
                string.Format("EVASPA_{0}.sql.gz", DateTime.Now.ToString("yyyyMMdd_HHmmss")));
        }

        [Authorize(Roles = "Admin")]
        public string Import(string confirm)
        {
            if (String.CompareOrdinal(confirm, "XaCnHaN") != 0)
            {
                return "Kí tự xác nhận không đúng!";
            }

            string sqlScript;
            try
            {
                HttpPostedFileBase file = Request.Files["inputfile"];
                if (file == null) throw new Exception("chưa chọn file");
                sqlScript = GZipDecompress(file.InputStream);
            }
            catch (Exception ex)
            {
                return "Lỗi " + ex.Message;
            }

            try
            {
                if (!string.IsNullOrWhiteSpace(sqlScript))
                {
                    RestoreDatabase(Dbname, sqlScript);
                    return "Nạp dữ liệu thành công.";
                }
                return "Dữ liệu bị lỗi hoặc không phù hợp.";
            }
            catch (Exception ex)
            {
                return "Lỗi: " + ex.Message;
            }
        }

        private void RestoreDatabase(string dbName, string script = "")
        {
            if (string.IsNullOrWhiteSpace(script))
                return;

            var server = new Server(new ServerConnection(new SqlConnection(ConnectionString)));
            var db = new Database(server, dbName);

            script = Regex.Replace(script, @"^--.*\r\n", "", RegexOptions.Multiline);
            string[] childrenScripts = Regex.Split(script, @"^GO", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            foreach (string s in childrenScripts)
            {
                db.ExecuteNonQuery(s);
            }
        }

        private string GenerateSqlScript(string dbName, string excludeTables = "")
        {
            var ret = new StringBuilder();
            var exclude = excludeTables.Split(',').Select(x => x.Trim());

            var server = new Server(new ServerConnection(new SqlConnection(ConnectionString)));
            Database evaspaMvc = server.Databases[dbName];

            // Get all talbes by dependency
            var walker = new DependencyWalker(server);
            Table[] tmpTables = evaspaMvc.Tables.OfType<Table>().ToArray();
            IEnumerable<Table> allTables;

            if (tmpTables.Any())
            {
                DependencyTree tree = walker.DiscoverDependencies(tmpTables, DependencyType.Children);
                DependencyCollection collection = walker.WalkDependencies(tree);
                allTables = tmpTables.Where(x => collection.ContainsUrn(x.Urn, server));
            }
            else
            {
                allTables = tmpTables;
            }

            var allForeignKeys = new List<ForeignKey>();
            List<StoredProcedure> storedProcedures = AllUserDefinedStoredProcedures(server, evaspaMvc);

            var optDrop = new ScriptingOptions
            {
                ScriptDrops = true,
                IncludeIfNotExists = true
            };

            var optSchemaOnly = new ScriptingOptions
            {
                DriPrimaryKey = true,
                DriForeignKeys = false
            };

            var optDataOnly = new ScriptingOptions
            {
                ScriptSchema = false,
                ScriptData = true,
                BatchSize = 50
            };

            // Create database if not existed
            ret.AppendLine("-- Create database if not existed");
            ret.AppendLine(
                string.Format("IF NOT EXISTS(SELECT name FROM sys.databases WHERE name = '{0}') CREATE DATABASE {0}",
                    dbName));
            ret.AppendLine("GO");

            // Drop all foreign keys
            ret.AppendLine("-- Drop all foreign keys");
            foreach (Table table in allTables)
            {
                ret.AppendLine("-- Drop all foreign keys of table " + table.Name);
                foreach (ForeignKey key in table.ForeignKeys)
                {
                    // add foreign keys to list for generating create script later
                    allForeignKeys.Add(key);

                    // write drop foreign key script
                    ret.AppendLine("-- Drop foreign key " + key.Name);
                    foreach (string line in key.Script(optDrop))
                        ret.AppendLine(
                            string.Format(
                                "IF  EXISTS (SELECT * FROM [{0}].sys.foreign_keys WHERE object_id = OBJECT_ID(N'[{0}].[{1}].[{3}]') AND parent_object_id = OBJECT_ID(N'[{0}].[{1}].[{2}]'))" +
                                " ALTER TABLE [{0}].[{1}].[{2}] DROP CONSTRAINT [{3}]",
                                dbName, table.Schema, table.Name, key.Name));
                    ret.AppendLine("GO");
                }
            }

            // Drop all tables
            ret.AppendLine("-- Drop all tables");
            foreach (Table table in allTables)
            {
                ret.AppendLine("-- Drop table " + table.Name);
                ret.AppendLine(
                    string.Format(
                        "IF  EXISTS (SELECT * FROM [{0}].sys.objects WHERE object_id = OBJECT_ID(N'[{0}].[{1}].[{2}]') AND type in (N'U')) DROP TABLE [{0}].[{1}].[{2}]",
                        dbName, table.Schema, table.Name));
                ret.AppendLine("GO");
            }

            // Drop all stored procedures
            ret.AppendLine("-- Drop all stored procedures");
            foreach (StoredProcedure sp in storedProcedures)
            {
                ret.AppendLine("-- Drop stored procedure " + sp.Name);
                ret.AppendLine(
                    string.Format(
                        "EXEC('USE [{0}] IF  EXISTS (SELECT * FROM [{0}].sys.objects WHERE object_id = OBJECT_ID(N''[{0}].[{1}].[{2}]'') AND type in (N''P'', N''PC'')) DROP PROCEDURE [{1}].[{2}]')",
                        dbName, sp.Schema, sp.Name));
                ret.AppendLine("GO");
            }

            ret.AppendLine("USE " + dbName);
            ret.AppendLine("GO");

            // Create tables
            ret.AppendLine("-- Create tables");
            foreach (Table table in allTables)
            {
                ret.AppendLine("-- Create table " + table.Name);
                foreach (string line in table.EnumScript(optSchemaOnly))
                    ret.AppendLine(line);
                ret.AppendLine("GO");
            }

            // Add data
            ret.AppendLine("-- Add data");
            foreach (Table table in allTables)
            {
                ret.AppendLine("-- Add data for table " + table.Name);

                if (exclude.Any(x => string.Compare(x, table.Name, StringComparison.OrdinalIgnoreCase) == 0))
                {
                    ret.AppendLine("-- Ignore");
                    continue;
                }

                int index = 1;
                foreach (string line in table.EnumScript(optDataOnly))
                {
                    ret.AppendLine(line);
                    if (index % BatchSize == 0)
                    {
                        ret.AppendLine("--Insert records index " + index + " for table " + table.Name);
                        ret.AppendLine("GO");
                    }
                    index++;
                }
                ret.AppendLine("GO");
            }

            // Create foreign keys
            ret.AppendLine("-- Create foreign keys");
            foreach (ForeignKey key in allForeignKeys)
            {
                ret.AppendLine("-- Create foreign key " + key.Name + " in table " + key.Parent.Name);
                foreach (string line in key.Script())
                    ret.AppendLine(line);
                ret.AppendLine("GO");
            }

            // Create stored procedures
            ret.AppendLine("-- Create stored procedures");
            foreach (StoredProcedure sp in storedProcedures)
            {
                ret.AppendLine("-- Create stored procedure " + sp.Name);
                foreach (string line in sp.Script())
                {
                    if (line.StartsWith("CREATE PROCEDURE", StringComparison.OrdinalIgnoreCase))
                    {
                        ret.AppendLine("GO\r\n" + line);
                    }
                    else
                    {
                        ret.AppendLine(line);
                    }
                }
                ret.AppendLine("GO");
            }

            return ret.ToString();
        }

        private List<StoredProcedure> AllUserDefinedStoredProcedures(Server server, Database db)
        {
            var ret = new List<StoredProcedure>();

            DataTable table = db.EnumObjects(DatabaseObjectTypes.StoredProcedure);
            foreach (DataRow row in table.Rows)
            {
                var sSchema = (string)row["Schema"];
                if (sSchema == "sys" || sSchema == "INFORMATION_SCHEMA")
                    continue;
                var sp = (StoredProcedure)server.GetSmoObject(
                    new Urn((string)row["Urn"]));
                if (!sp.IsSystemObject)
                    ret.Add(sp);
            }

            return ret;
        }

        private Stream GZipCompress(string xmlContent)
        {
            var input = new MemoryStream(Encoding.UTF8.GetBytes(xmlContent));
            var output = new MemoryStream();
            using (var gzip = new GZipStream(output, CompressionMode.Compress))
            {
                input.CopyTo(gzip);
            }
            return new MemoryStream(output.ToArray());
        }

        private string GZipDecompress(Stream input)
        {
            byte[] output;

            using (var gzip = new GZipStream(input, CompressionMode.Decompress))
            {
                const int size = 4096;
                var buffer = new byte[size];
                using (var memory = new MemoryStream())
                {
                    int count;
                    do
                    {
                        count = gzip.Read(buffer, 0, size);
                        if (count > 0)
                        {
                            memory.Write(buffer, 0, count);
                        }
                    } while (count > 0);
                    output = memory.ToArray();
                }
            }

            return Encoding.UTF8.GetString(output);
        }
	}
}