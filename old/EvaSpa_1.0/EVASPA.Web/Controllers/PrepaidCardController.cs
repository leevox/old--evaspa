﻿using EVASPA.Core.Service;
using EVASPA.Model.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace EVASPA.Web.Controllers
{
    public class PrepaidCardController : ControllerBase
    {
        private IPrepaidCardService PrepaidCardService { get; set; }
        private IUserService UserService { get; set; }
        private IItemService ItemService { get; set; }

        public PrepaidCardController(IPrepaidCardService prepaidCardService, IUserService userService, IItemService itemService)
        {
            PrepaidCardService = prepaidCardService;
            UserService = userService;
            ItemService = itemService;
        }

        // GET: /PrepaidCard/
        public ActionResult Index()
        {
            var model = new List<PrepaidCard>();

            var allCards = PrepaidCardService.AllCardsWithPreLoadedInfo.OrderBy(x => x.Customer.Name).ToList();
            var PrepaidCardStatuses = new Dictionary<int, string>();
            foreach (var card in allCards)
            {
                if (card.IsLocked)
                    continue;

                var available = PrepaidCardService.GetCardAvailable(card);
                if (available > 0)
                {
                    PrepaidCardStatuses.Add(card.Id, string.Format("{0:#,0.##} / {1:#,0.##}", card.Quota - available, card.Quota));
                    model.Add(card);
                }
            }
            ViewBag.PrepaidCardStatuses = PrepaidCardStatuses;
            return View(model);
        }

        public ActionResult ExpiredCards()
        {
            var model = new List<PrepaidCard>();

            var allCards = PrepaidCardService.AllCardsWithPreLoadedInfo.OrderBy(x => x.Customer.Name).ToList();
            var PrepaidCardStatuses = new Dictionary<int, string>();
            foreach (var card in allCards)
            {
                var available = PrepaidCardService.GetCardAvailable(card);
                if (available <= 0 || card.IsLocked)
                {
                    PrepaidCardStatuses.Add(card.Id, string.Format("{0:#,0} / {1:#,0}", card.Quota - available, card.Quota));
                    model.Add(card);
                }
            }
            ViewBag.PrepaidCardStatuses = PrepaidCardStatuses;
            return View(model);
        }

        // GET: /PrepaidCard/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PrepaidCard prepaidcard = PrepaidCardService.GetById(id);
            if (prepaidcard == null)
            {
                return HttpNotFound();
            }
            return View(prepaidcard);
        }

        public ActionResult DetailsBySerial(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PrepaidCard prepaidcard = PrepaidCardService.GetBySerialNumber(id);
            if (prepaidcard == null)
            {
                return HttpNotFound();
            }
            return View("Details", prepaidcard);
        }

        // GET: /PrepaidCard/Delete/5
        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PrepaidCard prepaidcard = PrepaidCardService.GetById(id);
            if (prepaidcard == null)
            {
                return HttpNotFound();
            }
            return View(prepaidcard);
        }

        // POST: /PrepaidCard/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,Manager")]
        public ActionResult DeleteConfirmed(int id)
        {
            //PrepaidCard prepaidcard = PrepaidCardService.GetById(id);
            //db.PrepaidCards.Remove(prepaidcard);
            //db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult UseMoneyCard(string id)
        {
            var success = true;
            var msg = string.Empty;
            var available = 0M;
            var cardId = 0;
            if (!string.IsNullOrWhiteSpace(id))
            {
                try
                {
                    id = id.Trim();
                    var card = PrepaidCardService.AllMoneyCards.FirstOrDefault(
                        x => string.Compare(x.SerialNumber, id, StringComparison.OrdinalIgnoreCase) == 0);
                    if (card == null)
                    {
                        throw new Exception("Mã phiếu không đúng");
                    }
                    cardId = card.Id;
                    available = PrepaidCardService.GetCardAvailable(card);
                    if (available <= 0)
                    {
                        throw new Exception("Phiếu đã hết hạn mức sử dụng.");
                    }
                }
                catch (Exception ex)
                {
                    success = false;
                    msg = ex.Message;
                }
            }
            return Json(new { success, msg, available, cardId });
        }

        public ActionResult UseServiceCard(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                return View("_SelectCard");

            var prepaidcard = PrepaidCardService.GetBySerialNumber(id);
            if (prepaidcard == null)
            {
                ViewBag.Msg = "Phiếu '" + id + "' không tồn tại.";
                return View("_SelectCard");
            }
            if (PrepaidCardService.GetCardAvailable(prepaidcard) <= 0)
            {
                ViewBag.Msg = "Phiếu '" + id + "' đã được sử dụng hết.";
                return View("_SelectCard");
            }
            ViewBag.PrepaidCard = prepaidcard;
            ViewBag.AllEmployees = UserService.AllEmployees;
            ViewBag.AllProducts = ItemService.GetAllProducts();
            return View();
        }

        [HttpPost]
        public ActionResult UseServiceCard(PrepaidServiceCardHistory model, string usedDate, int[] PerformerIds)
        {
            var success = false;
            var msg = "";

            try
            {
                model.Date = DateTime.ParseExact(usedDate, "dd/MM/yyyy", null);
                PrepaidCardService.UseServiceCard(model, PerformerIds);

                success = true;
                msg = Url.Action("Details", new { id = model.PrepaidServiceCardId });
            }
            catch (Exception ex)
            {
                success = false;
                msg = ex.Message;
            }


            return Json(new {success, msg });
        }

        public ActionResult LockCard(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PrepaidCard prepaidcard = PrepaidCardService.GetById(id);
            if (prepaidcard == null)
            {
                return HttpNotFound();
            }
            ViewBag.CardSerial = prepaidcard.SerialNumber;
            ViewBag.IsCardLocked = prepaidcard.IsLocked;
            return View();
        }

        [HttpPost, ActionName("LockCard")]
        public ActionResult LockCardConfirmed(string id)
        {
            PrepaidCardService.LockCard(id);
            return RedirectToAction("DetailsBySerial", new { id });
        }

        [Authorize(Roles = "Admin,Manager")]
        public ActionResult DeleteHistory(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.Id = id;
            return View();
        }

        [HttpPost, ActionName("DeleteHistory")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,Manager")]
        public ActionResult DeleteHistoryConfirmed(int id)
        {
            var ret = PrepaidCardService.DeleteHistory(id);
            return RedirectToAction("Details", new { id = ret });
        }
    }
}
