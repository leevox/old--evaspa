﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using EVASPA.Model.DataModel;

namespace EVASPA.Web.Utils
{
    public static class EvaSpaHelper
    {
        public static IHtmlString EvaSpa_GetCustomerName(this HtmlHelper htmlHelper, Customer customer)
        {
            return EvaSpa_GetCustomerName(htmlHelper, customer, string.Empty);
        }

        public static IHtmlString EvaSpa_GetCustomerName(this HtmlHelper htmlHelper, Customer customer, string prefix)
        {
            if (customer == null)
                return null;
            return
                htmlHelper.Raw(prefix + (customer.IsVip
                    ? string.Format("<span class='customer-vip'>{0} [VIP]</span>", customer.Name)
                    : customer.Name));
        }

        public static bool IsInRoles(this System.Security.Principal.IPrincipal principal, params string[] roles)
        {
            return roles.Any(principal.IsInRole);
        }
    }
}