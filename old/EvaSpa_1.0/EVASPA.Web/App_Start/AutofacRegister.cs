﻿using Autofac;
using Autofac.Integration.Mvc;
using EVASPA.Core.Repository;
using EVASPA.Core.Service;
using EVASPA.Core.UnitOfWork;
using EVASPA.Data;
using EVASPA.Repository;
using EVASPA.Service;
namespace EVASPA.Web
{
    public class AutofacRegister
    {
        private ContainerBuilder Builder { get; set; }

        public void Reg(ContainerBuilder builder)
        {
            Builder = builder;

            RegCommonUnits();
            RegServices();
            RegRepositories();
        }

        public void RegRepositories()
        {
            Builder.RegisterType<UserRepository>().As<IUserRepository>();
            Builder.RegisterType<CustomerRepository>().As<ICustomerRepository>();
            Builder.RegisterType<BillRepository>().As<IBillRepository>();
            Builder.RegisterType<BillItemRepository>().As<IBillItemRepository>();
            Builder.RegisterType<DebtRepository>().As<IDebtRepository>();
            Builder.RegisterType<DebtHistoryRepository>().As<IDebtHistoryRepository>();
            Builder.RegisterType<ItemRepository>().As<IItemRepository>();
            Builder.RegisterType<ItemHistoryRepository>().As<IItemHistoryRepository>();
            Builder.RegisterType<LeavingRepository>().As<ILeavingRepository>();
            Builder.RegisterType<PrepaidCardRepository>().As<IPrepaidCardRepository>();
            Builder.RegisterType<PrepaidCardHistoryRepository>().As<IPrepaidCardHistoryRepository>();
            Builder.RegisterType<ProductsUsedInServiceRepository>().As<IProductsUsedInServiceRepository>();
        }

        public void RegServices()
        {
            Builder.RegisterType<UserService>().As<IUserService>();
            Builder.RegisterType<CustomerService>().As<ICustomerService>();
            Builder.RegisterType<BillService>().As<IBillService>();
            Builder.RegisterType<BillItemService>().As<IBillItemService>();
            Builder.RegisterType<DebtService>().As<IDebtService>();
            Builder.RegisterType<DebtHistoryService>().As<IDebtHistoryService>();
            Builder.RegisterType<ItemService>().As<IItemService>();
            Builder.RegisterType<ItemHistoryService>().As<IItemHistoryService>();
            Builder.RegisterType<LeavingService>().As<ILeavingService>();
            Builder.RegisterType<PrepaidCardService>().As<IPrepaidCardService>();
            Builder.RegisterType<PrepaidCardHistoryService>().As<IPrepaidCardHistoryService>();
            Builder.RegisterType<ReportService>().As<IReportService>();
        }

        public void RegCommonUnits()
        {
            Builder.RegisterType<EvaContext>().As<IUnitOfWork>().As<IEvaContext>().InstancePerHttpRequest();
        }   
    }
}