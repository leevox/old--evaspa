﻿using EVASPA.Core.Repository;
using EVASPA.Core.UnitOfWork;
using EVASPA.Model.DataModel;

namespace EVASPA.Repository
{
    public class DebtRepository : Repository<Debt>, IDebtRepository
    {
        public DebtRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}