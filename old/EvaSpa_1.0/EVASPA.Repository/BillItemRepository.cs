using EVASPA.Core.Repository;
using EVASPA.Core.UnitOfWork;
using EVASPA.Model.DataModel;

namespace EVASPA.Repository
{
    public class BillItemRepository : Repository<BillItem>, IBillItemRepository
    {
        public BillItemRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}