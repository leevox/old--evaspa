﻿using EVASPA.Core.Repository;
using EVASPA.Core.UnitOfWork;
using EVASPA.Model.DataModel;

namespace EVASPA.Repository
{
    public class ItemHistoryRepository : Repository<ItemHistory>, IItemHistoryRepository
    {
        public ItemHistoryRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}