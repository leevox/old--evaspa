using EVASPA.Core.Repository;
using EVASPA.Core.UnitOfWork;
using EVASPA.Model.DataModel;

namespace EVASPA.Repository
{
    public class LeavingRepository : Repository<Leaving>, ILeavingRepository
    {
        public LeavingRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}