﻿using System.Data.Entity;
using EVASPA.Core.Repository;
using EVASPA.Core.UnitOfWork;
using EVASPA.Model.DataModel;
using System.Linq;

namespace EVASPA.Repository
{
    public class PrepaidCardRepository : Repository<PrepaidCard>, IPrepaidCardRepository
    {
        public PrepaidCardRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public IQueryable<PrepaidMoneyCard> AllPrepaidMoneyCards
        {
            get { return AllEntities.OfType<PrepaidMoneyCard>(); }
        }


        public IQueryable<PrepaidMoneyCard> AllPrepaidMoneyCardsWithHistories
        {
            get
            {
                return UnitOfWork.GetEntities<PrepaidMoneyCard>().Include(x => x.History).Where(x => !x.IsDeleted);
            }
        }


        public IQueryable<PrepaidCard> AllPrepaidCardsWithPreLoadedInfo
        {
            get
            {
                return UnitOfWork.GetEntities<PrepaidCard>().Include(x => x.CardType).Include(x => x.Customer).Where(x => !x.IsDeleted);
            }
        }
    }
}