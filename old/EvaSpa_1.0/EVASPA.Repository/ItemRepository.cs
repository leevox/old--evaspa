﻿using System.Linq;
using EVASPA.Core.Repository;
using EVASPA.Core.UnitOfWork;
using EVASPA.Model.DataModel;

namespace EVASPA.Repository
{
    public class ItemRepository : Repository<Item>, IItemRepository
    {
        public ItemRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public IQueryable<Product> AllProducts
        {
            get { return AllEntities.OfType<Product>(); }
        }

        public IQueryable<Service> AllServices
        {
            get { return AllEntities.OfType<Service>(); }
        }

        public IQueryable<CardType> AllCardTypes
        {
            get { return AllEntities.OfType<CardType>(); }
        }
    }
}