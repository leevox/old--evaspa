﻿using EVASPA.Core.Repository;
using EVASPA.Core.UnitOfWork;
using EVASPA.Model.DataModel;

namespace EVASPA.Repository
{
    public class ProductsUsedInServiceRepository : Repository<ProductsUsedInService>, IProductsUsedInServiceRepository
    {
        public ProductsUsedInServiceRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
