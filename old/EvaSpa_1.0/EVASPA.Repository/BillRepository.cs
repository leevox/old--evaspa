﻿using EVASPA.Core.Repository;
using EVASPA.Core.UnitOfWork;
using EVASPA.Model.DataModel;

namespace EVASPA.Repository
{
    public class BillRepository : Repository<Bill>, IBillRepository
    {
        public BillRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}