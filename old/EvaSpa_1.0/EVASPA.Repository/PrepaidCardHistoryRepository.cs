﻿using EVASPA.Core.Repository;
using EVASPA.Core.UnitOfWork;
using EVASPA.Model.DataModel;

namespace EVASPA.Repository
{
    public class PrepaidCardHistoryRepository : Repository<PrepaidCardHistory>, IPrepaidCardHistoryRepository
    {
        public PrepaidCardHistoryRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}