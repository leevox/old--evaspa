﻿using EVASPA.Core.Repository;
using EVASPA.Core.UnitOfWork;
using EVASPA.Model.DataModel;
using System.Data.Entity;
using System.Linq;

namespace EVASPA.Repository
{
    public class CustomerRepository : Repository<Customer>, ICustomerRepository
    {
        public CustomerRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public IQueryable<Customer> AllCustomersWithDebt()
        {
            return AllEntities.Include("Debts").Include("DebtHistories");
        }
    }
}
