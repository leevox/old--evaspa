﻿using EVASPA.Core.Repository;
using EVASPA.Core.UnitOfWork;
using EVASPA.Model.DataModel;

namespace EVASPA.Repository
{
    public class DebtHistoryRepository : Repository<DebtHistory>, IDebtHistoryRepository
    {
        public DebtHistoryRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}