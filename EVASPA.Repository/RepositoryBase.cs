﻿using System.Collections.Generic;
using EVASPA.Model;
using System;

namespace EVASPA.Repository
{
    public abstract class RepositoryBase<T> where T : EntityBase
    {
        public T Get(int id, bool includeDeleted = false)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<T> Get(IEnumerable<int> ids, bool includeDeleted = false)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<T> GetAll(bool includeDeleted = false)
        {
            throw new NotImplementedException();
        }

        public T Add(T entity)
        {
            throw new NotImplementedException();
        }

        public T AddOrUpdate(T entity)
        {
            throw new NotImplementedException();
        }

        public T Update(T entity)
        {
            throw new NotImplementedException();
        }

        public void Remove(T entity)
        {
            entity.DeletedDate = DateTime.UtcNow;
            Update(entity);
        }
    }
}
