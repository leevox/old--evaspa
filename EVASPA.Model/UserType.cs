﻿
namespace EVASPA.Model
{
    public enum UserType
    {
        Employee = 0,
        Manager = 1,
        Admin = 2
    }
}
