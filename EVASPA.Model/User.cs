﻿namespace EVASPA.Model
{
    public class User : EntityBase
    {
        public string UserName { get; set; }

        public string PasswordHash { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public UserType Type { get; set; }
    }
}
